# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Factories' do
  describe 'User factory' do
    it 'creates a user' do
      expect { create(:user) }.to change(User, :count).by(1)
    end
  end
end
