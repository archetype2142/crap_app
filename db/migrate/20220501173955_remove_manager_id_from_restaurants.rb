class RemoveManagerIdFromRestaurants < ActiveRecord::Migration[7.0]
  def change
    remove_column :restaurants, :manager_id, :bigint
  end
end
