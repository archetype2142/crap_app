class CreateConfigs < ActiveRecord::Migration[7.0]
  def change
    create_table :configs do |t|
      t.string :key, null: false, unique: true, index: true
      t.string :key_type, null: false
      t.text :value, null: false
      t.boolean :needs_auth, default: true, null: false
      t.boolean :internal, default: true
      t.text :description
      t.text :example

      t.timestamps
    end
  end
end
