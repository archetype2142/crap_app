class CreateRestaurantManagers < ActiveRecord::Migration[7.0]
  def change
    create_table :restaurant_users do |t|
      t.references :restaurant
      t.references :user
    end
  end
end
