class CreateOrder < ActiveRecord::Migration[7.0]
  def change
    create_enum :dining_options, ["dine_in", "home_delivery", "take_out"]

    create_table :orders do |t|
      t.string :order_uuid
      t.integer :order_total, default: 0, null: false
      t.boolean :completed, default: false
      t.integer :tips_total, default: 0, null: false
      t.integer :discount_amount, default: 0, null: false
      t.enum :dining_option, enum_type: "dining_options", default: "dine_in", null: false
      t.references :restaurant

      t.timestamps
    end
  end
end
