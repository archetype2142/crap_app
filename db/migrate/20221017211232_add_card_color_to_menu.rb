class AddCardColorToMenu < ActiveRecord::Migration[7.0]
  def change
    add_column :menus, :card_color, :string
  end
end
