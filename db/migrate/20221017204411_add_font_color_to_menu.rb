class AddFontColorToMenu < ActiveRecord::Migration[7.0]
  def change
    add_column :menus, :font_color, :string
  end
end
