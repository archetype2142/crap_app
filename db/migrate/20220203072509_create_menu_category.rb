# frozen_string_literal: true

class CreateMenuCategory < ActiveRecord::Migration[7.0]
  def change
    create_table :menu_categories do |t|
      t.references :menu
      t.references :category

      t.timestamps
    end
  end
end
