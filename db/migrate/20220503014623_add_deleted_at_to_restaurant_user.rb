class AddDeletedAtToRestaurantUser < ActiveRecord::Migration[7.0]
  def change
    add_column :restaurant_users, :deleted_at, :datetime
  end
end
