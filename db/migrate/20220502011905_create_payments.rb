class CreatePayments < ActiveRecord::Migration[7.0]
  def change
    create_table :payments do |t|
      t.references :order
      t.integer :status, default: 0
      t.integer :amount, default: 0, null: false

      t.timestamps
    end
  end
end
