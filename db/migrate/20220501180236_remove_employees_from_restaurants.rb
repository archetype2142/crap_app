class RemoveEmployeesFromRestaurants < ActiveRecord::Migration[7.0]
  def change
    remove_column :restaurants, :employee_ids, :bigint
  end
end
