class AddMenuItemsToRestaurants < ActiveRecord::Migration[7.0]
  def change
    add_column :restaurants, :sidebar_menu_items, :string, array: true
  end
end
