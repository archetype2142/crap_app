class AddAggregatedMenuColumnsToRestaurant < ActiveRecord::Migration[7.0]
  def change
    add_column :restaurants, :aggregated_menu_background, :string
    add_column :restaurants, :aggregated_menu_animation, :string
    add_column :restaurants, :aggregated_menu_title, :string
    add_column :restaurants, :aggregated_menu_footer, :string
    add_column :restaurants, :aggregated_menu_font, :string
  end
end
