# frozen_string_literal: true

class CreateRestaurants < ActiveRecord::Migration[7.0]
  def change
    create_table :restaurants do |t|
      t.string :name
      t.string :image
      t.string :city
      t.references :manager, class_name: '::User'
      t.references :user

      t.timestamps
    end
  end
end
