# frozen_string_literal: true

class AddMenuToItem < ActiveRecord::Migration[7.0]
  def change
    add_reference :items, :menu
  end
end
