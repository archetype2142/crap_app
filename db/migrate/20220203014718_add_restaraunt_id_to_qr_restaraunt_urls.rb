# frozen_string_literal: true

class AddRestarauntIdToQrRestarauntUrls < ActiveRecord::Migration[7.0]
  def change
    add_reference :qr_restaraunt_urls, :restaurant, index: true
  end
end
