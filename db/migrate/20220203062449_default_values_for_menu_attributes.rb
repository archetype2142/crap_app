# frozen_string_literal: true

class DefaultValuesForMenuAttributes < ActiveRecord::Migration[7.0]
  def change
    change_column :menus, :background_color, :string, default: '#f5f0f0'
    change_column :menus, :font, :string, default: 'Nunito'
  end
end
