class AddEmployeesToRestaurant < ActiveRecord::Migration[7.0]
  def change
    add_column :restaurants, :employee_ids, :bigint
    add_index :restaurants, :employee_ids
  end
end
