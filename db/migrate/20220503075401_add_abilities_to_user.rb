class AddAbilitiesToUser < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :abilities, :string, array: true
  end
end
