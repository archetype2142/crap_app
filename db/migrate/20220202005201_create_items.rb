# frozen_string_literal: true

class CreateItems < ActiveRecord::Migration[7.0]
  def change
    create_table :items do |t|
      t.string :title
      t.text :description
      t.integer :price, default: 0, null: false
      t.text :ingredients
      t.references :menu

      t.timestamps
    end
  end
end
