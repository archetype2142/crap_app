class AddReceiverIdToUser < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :receiver_id, :bigint, index: true
    add_foreign_key :users, :referrals, column: :receiver_id
  end
end
