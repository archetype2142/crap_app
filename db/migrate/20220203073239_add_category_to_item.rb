# frozen_string_literal: true

class AddCategoryToItem < ActiveRecord::Migration[7.0]
  def change
    add_reference :items, :category, index: true
  end
end
