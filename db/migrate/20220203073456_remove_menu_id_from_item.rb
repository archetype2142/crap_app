# frozen_string_literal: true

class RemoveMenuIdFromItem < ActiveRecord::Migration[7.0]
  def change
    remove_column :items, :menu_id
  end
end
