class CreateReferrals < ActiveRecord::Migration[7.0]
  def change
    create_table :referrals do |t|
      t.integer :sender_id
      t.string :email
      t.string :message
      t.string :type
      t.string :first_name
      t.string :last_name
      t.bigint :relate_resource_id
      t.string :relate_resource_type
      t.references :receiver

      t.timestamps
    end
  end
end
