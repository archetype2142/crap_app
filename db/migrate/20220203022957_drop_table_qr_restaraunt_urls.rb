# frozen_string_literal: true

class DropTableQrRestarauntUrls < ActiveRecord::Migration[7.0]
  def change
    drop_table :qr_restaraunt_urls
  end
end
