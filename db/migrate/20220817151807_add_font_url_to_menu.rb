class AddFontUrlToMenu < ActiveRecord::Migration[7.0]
  def change
    add_column :menus, :font_url, :string
  end
end
