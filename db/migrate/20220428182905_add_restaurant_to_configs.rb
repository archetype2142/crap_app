class AddRestaurantToConfigs < ActiveRecord::Migration[7.0]
  def change
    add_reference :configs, :restaurant, index: true
    add_foreign_key :configs, :restaurants
  end
end
