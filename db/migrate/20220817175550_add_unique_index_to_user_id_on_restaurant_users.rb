class AddUniqueIndexToUserIdOnRestaurantUsers < ActiveRecord::Migration[7.0]
  def change
    remove_index :restaurant_users, :restaurant_id
    remove_index :restaurant_users, :user_id

    add_index :restaurant_users, [:restaurant_id, :user_id], unique: true
  end
end
