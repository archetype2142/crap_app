class CreateUserRegistrationTokens < ActiveRecord::Migration[7.0]
  def change
    create_table :user_registration_tokens do |t|
      t.string :token, index: true
      t.string :email
      t.datetime :validity
      t.datetime :used_at
    end
  end
end
