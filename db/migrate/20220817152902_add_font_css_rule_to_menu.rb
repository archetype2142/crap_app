class AddFontCssRuleToMenu < ActiveRecord::Migration[7.0]
  def change
    add_column :menus, :font_css_rule, :string
  end
end
