class AddUserTypeToRestaurantUser < ActiveRecord::Migration[7.0]
  def change
    add_column :restaurant_users, :user_type, :string
  end
end
