class CreateTables < ActiveRecord::Migration[7.0]
  def change
    create_table :tables do |t|
      t.string :name
      t.string :table_uuid
      t.integer :seats_available
      t.references :restaurant
    end
  end
end
