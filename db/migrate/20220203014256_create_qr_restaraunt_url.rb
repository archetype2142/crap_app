# frozen_string_literal: true

class CreateQrRestarauntUrl < ActiveRecord::Migration[7.0]
  def change
    create_table :qr_restaraunt_urls do |t|
      t.string :uuid
      t.string :url

      t.timestamps
    end
  end
end
