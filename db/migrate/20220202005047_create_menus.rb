# frozen_string_literal: true

class CreateMenus < ActiveRecord::Migration[7.0]
  def change
    create_table :menus do |t|
      t.string :background_color
      t.string :font
      t.string :title
      t.string :footer_text
      t.string :qr_code
      t.text :description
      t.references :restaurant

      t.timestamps
    end
  end
end
