class AddCardStylingToRestaurants < ActiveRecord::Migration[7.0]
  def change
    add_column :restaurants, :aggregated_menu_card_color, :string
  end
end
