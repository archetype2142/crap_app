class AddRestaurantToUser < ActiveRecord::Migration[7.0]
  def change
    add_reference :users, :restaurant
  end
end
