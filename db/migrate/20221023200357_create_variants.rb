class CreateVariants < ActiveRecord::Migration[7.0]
  def change
    create_table :variants do |t|
      t.string :name
      t.string :description
      t.integer :price, default: 0, null: false
      t.references :item
    end
  end
end
