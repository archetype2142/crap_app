class UpdateAllDeletedAtToDiscardedAt < ActiveRecord::Migration[7.0]
  def change
    rename_column :restaurant_users, :deleted_at, :discarded_at
    rename_column :restaurants, :deleted_at, :discarded_at
    rename_column :users, :deleted_at, :discarded_at
  end
end
