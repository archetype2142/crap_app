# frozen_string_literal: true

class AddSlugToMenu < ActiveRecord::Migration[7.0]
  def change
    add_column :menus, :slug, :string
  end
end
