class CreateLineItems < ActiveRecord::Migration[7.0]
  def change
    create_table :line_items do |t|
      t.references :item
      t.integer :quantity, default: 1, null: false
      t.integer :total, default: 0, null: false

      t.timestamps
    end
  end
end
