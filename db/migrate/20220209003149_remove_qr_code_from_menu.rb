class RemoveQrCodeFromMenu < ActiveRecord::Migration[7.0]
  def change
    remove_column :menus, :qr_code, :string
  end
end
