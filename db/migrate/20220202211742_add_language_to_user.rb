# frozen_string_literal: true

class AddLanguageToUser < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :language, :integer
  end
end
