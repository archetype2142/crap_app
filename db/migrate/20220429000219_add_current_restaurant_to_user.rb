class AddCurrentRestaurantToUser < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :current_restaurant_id, :bigint
  end
end
