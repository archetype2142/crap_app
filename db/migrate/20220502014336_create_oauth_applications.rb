class CreateOauthApplications < ActiveRecord::Migration[7.0]
  def change
    create_table :oauth_applications do |t|
      t.string "name"
      t.text "redirect_uri"
      t.string "scopes", default: "", null: false
      t.boolean "confidential", default: true, null: false
      t.text "title"
      t.text "description"
      t.text "url"
      t.string "client_id", null: false
      t.string "client_secret", null: false

      t.timestamps
    end
  end
end
