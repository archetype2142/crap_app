# frozen_string_literal: true

class CreateQrUrls < ActiveRecord::Migration[7.0]
  def change
    create_table :qr_urls do |t|
      t.string :owner_type
      t.bigint :owner_id
      t.string :uuid, index: true
      t.string :url

      t.timestamps
    end
  end
end
