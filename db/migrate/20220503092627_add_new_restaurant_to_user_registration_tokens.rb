class AddNewRestaurantToUserRegistrationTokens < ActiveRecord::Migration[7.0]
  def change
    add_column :user_registration_tokens, :new_restaurant, :boolean, default: false
  end
end
