# frozen_string_literal: true

class CreateItemTags < ActiveRecord::Migration[7.0]
  def change
    create_table :item_tags do |t|
      t.references :item
      t.references :tag

      t.timestamps
    end
  end
end
