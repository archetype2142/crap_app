class AddTableToOrder < ActiveRecord::Migration[7.0]
  def change
    add_reference :orders, :table
  end
end
