class AddLineItemsToOrder < ActiveRecord::Migration[7.0]
  def change
    add_reference :line_items, :order
  end
end
