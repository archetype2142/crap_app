# frozen_string_literal: true

class AddTemplateToMenu < ActiveRecord::Migration[7.0]
  def change
    add_column :menus, :template, :string, default: 'default'
  end
end
