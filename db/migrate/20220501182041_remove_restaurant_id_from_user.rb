class RemoveRestaurantIdFromUser < ActiveRecord::Migration[7.0]
  def change
    remove_column :users, :restaurant_id, :bigint
  end
end
