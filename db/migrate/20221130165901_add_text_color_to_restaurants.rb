class AddTextColorToRestaurants < ActiveRecord::Migration[7.0]
  def change
    add_column :restaurants, :aggregated_menu_text_color, :string
  end
end
