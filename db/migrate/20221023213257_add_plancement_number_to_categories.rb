class AddPlancementNumberToCategories < ActiveRecord::Migration[7.0]
  def change
    add_column :categories, :placement_number, :integer
  end
end
