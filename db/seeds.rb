# frozen_string_literal: true

User.all.destroy_all
Manager.all.destroy_all
Restaurant.all.destroy_all

user = User.create!(
  email: 'test@test.com',
  password: 'test123',
  first_name: FFaker::Name.first_name,
  last_name: FFaker::Name.last_name,
  phone: FFaker::PhoneNumberPL.mobile_phone_number,
  language: :en
)

Restaurant.create!(name: 'test')

12.times do |x|
  restaurant = user.restaurants.create!(
    name: "restaurant ##{x}",
    city: FFaker::Address.city
  )

  3.times do |y|
    menu = restaurant.menus.create!(
      title: "menu #{y}"
    )

    15.times do |x|
      menu.items.create!(
        title: "item #{x}",
        description: FFaker::Lorem.paragraph,
        ingredients: Array.new(6) { FFaker::Food.ingredient }.join(', '),
        price: rand(1000)
      )
    end
  end
end

Manager.create!(
  email: 'manager@test.com',
  password: 'test123',
  first_name: FFaker::Name.first_name,
  last_name: FFaker::Name.last_name,
  phone: FFaker::PhoneNumberPL.mobile_phone_number,
  language: :en,
  restaurant_ids: [Restaurant.last.id]
)
if Rails.env.development?
  AdminUser.create!(email: 'admin@example.com', password: 'password',
                    password_confirmation: 'password')
end
