# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview
  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/mailer
  def invite_employee
    UserMailer.invite_employee(
      Restaurant.all.sample.id,
      'johndoe@example.com'
    )
  end

  def onboard_new_registration
    UserMailer.onboard_new_registration(
      FFaker::Lorem.word,
      SecureRandom.urlsafe_base64,
      'johndoe@example.com'
    )
  end
end
