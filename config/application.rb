# frozen_string_literal: true

require_relative 'boot'
require 'rails/all'

require_relative 'prepare_startup_yml'
require_relative '../app/models/startup_config'
# require 'lib/application_config_validator'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

def log_stdout_and_logger(msg)
  puts msg unless ENV['NO_OUPUT'].to_s.downcase == 'true'
  Rails.logger.info(msg) if Rails.logger.present?
end

module Menuizer
  class Application < Rails::Application
    config.load_defaults 7.0
    config.assets.css_compressor = nil
    config.i18n.available_locales = %i[en pl]
    config.i18n.default_locale = :en
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]
    config.active_record.default_timezone = :utc
    config.active_job.queue_adapter = :sidekiq

    config.after_initialize do
      # Do not run configs definition from Rake context
      # a) migrations that do not have "configs" db ready, will fail due to schema missing
      # b) rake tasks should not need this
      # unless $i_am_rake || Rails.env.test?
      #   log_stdout_and_logger("Configs: init (db-only)")
      #   configs = []
      #   time = Benchmark.realtime { configs = ApplicationConfigValidator.define_configs! }
      #   log_stdout_and_logger("Configs: complete: #{configs.size} in #{time.real.round(2)} s")
      # end
    end
  end
end
