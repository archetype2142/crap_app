module Routes
  module Admin
    def self.extended(router)
      router.instance_exec do
        root to: 'dashboard#index'

        resources :menus,               only: %i[index show update create destroy]
        resources :restaurants,         only: %i[index show update create destroy]
        resources :managers,            only: %i[index edit update create destroy]

        resources :categories,          only: %i[index update create destroy] do
          collection do
            post 'reorder-position' => "categories#reorder_position"
          end
        end

        resources :searches,            only: %i[index]
        resources :current_restaurants, only: %i[update]
        resources :tables,              only: %i[create index update destroy]
        resources :employees,           only: %i[create index update destroy]
        resources :dashboard,           only: %i[index]
        resources :orders,              only: %i[index]
        resources :aggregated_menus,    only: %i[index update]

        resources :referrals,           only: %i[create update] do
          collection do
            put 'resend-resource-email/:id' => "referrals#resend_referral_email", as: :resend_referral_email
          end
        end

        resources :settings,            only: %i[index] do
          collection do
            get 'link-stripe-account/:restaurant_id' => 'settings#link_stripe_account', as: :link_stripe_account
          end
        end

        resources :menus, module: :menus do
          resources :items, only: %i[index show update create destroy]
        end
      end
    end
  end
end
