module Routes
  module V1
    # Defines routes for V1 and newer API
    def self.extended(router)
      router.instance_exec do
        namespace :v1, path: '' do
          resource :settings, only: [:show, :update]

          resource :uber_auth, only: [] do
            collection do
              post 'login' => "uber_auth#login", as: :uber_auth_login
              # uber_auth_login_api_v1_uber_auth
            end
          end
        end
      end
    end
  end
end
