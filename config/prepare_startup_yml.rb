# frozen_string_literal: true

# This will be used in rake task to prepare startup.yml
# with configs that can be used in rails or sidekiq initialization
# before rails and database are loaded

class PrepareStartupYml
  DB_PATH = 'config/database.yml'
  EXPORT_PATH = "config/startup-#{Rails.env}.yml"

  def self.prepare
    configs_hash = nil
    configs_hash = load_from_db unless Rails.env.test?

    if File.exist?(EXPORT_PATH)
      puts "Expected to start without #{EXPORT_PATH}. Overriding. Making backup: #{EXPORT_PATH}.backup".in_yellow
      FileUtils.cp(EXPORT_PATH, EXPORT_PATH + '.backup')
    end

    File.open(EXPORT_PATH, 'w') { |f| f.write configs_hash.to_yaml }
    puts "Init: config saved to #{EXPORT_PATH}"
  end

  def self.load_from_db
    puts "Init: loading config from database with setup from #{DB_PATH}"
    db_yml = YAML.load_file(DB_PATH)[Rails.env]

    ActiveRecord::Base.establish_connection(
      adapter: db_yml['adapter'],
      database: db_yml['database'],
      username: db_yml['username'],
      password: db_yml['password'],
      port: db_yml['port'] || 5432,
      host: db_yml['host']
    )

    configs_hash = Config.all.inject({}) do |result_hash, config|
      result_hash.merge(
        config.key => config.safe_value
      )
    end
    puts 'Init: loading config from database completed'
    configs_hash
  rescue PG::ConnectionBad, ActiveRecord::NoDatabaseError => e
    puts "Init: database not accessible: #{e.class} #{e.message}"
    nil
  end
end
