# frozen_string_literal: true

# Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
require 'sidekiq/web'

Dir.glob('config/routes/v*').each do |version_routes|
  require Rails.root.join(version_routes)
end

require Rails.root.join('config/routes/admin')

Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  authenticate :user do
    mount Sidekiq::Web => '/sidekiq'
  end

  scope '(:locale)', locale: /en|pl/ do
    root 'static_pages#home'

    namespace :api do
      scope ':version', version: /v1/ do
        extend Routes::V1
      end
    end

    devise_for :users,
               class_name: '::User',
               path: 'admin',
               skip: %i[confirmations unlocks],
               controllers: {
                 sessions: 'admin/authentication/sessions',
                 registrations: 'admin/authentication/registrations'
               }

    namespace :admin do
      extend Routes::Admin
    end

    resources :restaurants, module: :restaurants do
      resources :menus, only: :show
    end

    resources :aggregated_menus, only: :index 
    resources :orders

    namespace :third_party do
      namespace :payments do
        resources :stripe_payments, only: [] do
          collection do
            get 'stripe_refresh'
            get 'stripe_return'
            post 'create_connected_account'
          end
        end
      end
    end

    resources :webhooks, only: [:create]
  end

  get 'redirect/:id', to: 'qr_redirects#show'
  post 'new-restaurants-registeration', to: 'new_registrations#create', as: :new_restaurant_registration
end
