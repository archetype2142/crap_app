require 'stripe'
require_relative '../../app/models/stripe_mode'

# You can enable automatic retries on requests that fail due to a transient problem
# by configuring the maximum number of retries:
Stripe.max_network_retries = 2

# The library can be configured to emit logging that will give you better insight
# into what it's doing. The info logging level is usually most appropriate for production use,
# but debug is also available for more verbosity.
Stripe.log_level = Stripe::LEVEL_INFO

# By default, the library sends request latency telemetry to Stripe.
# These numbers help Stripe improve the overall latency of its API for all users.
Stripe.enable_telemetry = false

if !Rails.env.test? && ::StripeMode.on?
  log_stdout_and_logger("Init: Stripe: Initializing..: #{StartupConfig.get_config('third_party.stripe.api_key').to_s[0..12]}")
  Stripe.api_key = StartupConfig.get_config('third_party.stripe.api_key')
else
  if $i_am_rspec
    ActiveRecord::Base.connection.raw_connection
  end
  log_stdout_and_logger('Init: Stripe: is off.')
end
