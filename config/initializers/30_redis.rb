# frozen_string_literal: true

REDIS = Redis.new(
  if ENV['REDIS_URL'].present?
    {
      namespace: ENV['REDIS_NAMESPACE'],
      url: ENV['REDIS_URL'],
      port: ENV['REDIS_PORT'],
      ssl_params: { verify_mode: OpenSSL::SSL::VERIFY_NONE }
    }
  else
    { url: 'redis://localhost:6379/' }
  end
)
