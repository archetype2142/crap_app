$i_am_rake_or_rspec = $0 =~ /(rake|rspec)/
$i_am_sidekiq = $0 =~ /(sidekiq)/ # similar to check: Sidekiq.server?
$i_am_rspec = $0 =~ /(rspec)/
$i_am_rake = $0 =~ /(rake)/
$i_am_console = Rails.const_defined?('Console')

Rails.logger.warn "Init: Executing: #{$0} #{$*}"
