# Support for colored strings in logs

class String
  COLORS = {
    blue:  34,
    yellow: 33,
    cyan: 36,
    green: 32,
    magenta: 35,
    red: 31,
    white: 37
  } unless const_defined?(:COLORS)

  COLORS.each do |color, code|
    define_method("in_#{color}".to_sym) do
      "\e[#{code}m#{self.gsub("\n", '')}\033[0m"
    end
  end

  def uncolorize
    self.gsub(/\[\d+m/m, '')
  end
end
