# frozen_string_literal: true

MoneyRails.configure do |config|
  config.default_currency = :pln
end

Money.locale_backend = :i18n
