# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.1.0'

gem 'activeadmin',          '~> 2.12.0'
gem 'aws-sdk-s3',           '~> 1.111.3'
gem 'bootsnap',             '~> 1.10.2',
    require: false
gem 'cssbundling-rails',    '~> 1.0.0'
gem 'devise',               '~> 4.8.1'
gem 'discard',              '~> 1.2.1'
gem 'dotenv-rails',         '>= 2.7.6'
gem 'image_processing',     '~> 1.12.2'
gem 'inky-rb',              '>= 1.4.2.0',
    require: 'inky'
gem 'jbuilder',             '~> 2.11.5'
gem 'jsbundling-rails',     '~> 1.0.0'
gem 'kaminari',             '~> 1.2.2'
gem 'mobility',             '~> 1.2.6'
gem 'mobility-ransack',     '~> 1.2.1'
gem 'money-rails',          '~> 1.15.0'
gem 'pg',                   '~> 1.3.5'
gem 'premailer-rails',      '~> 1.11.1'
gem 'puma',                 '~> 5.6.4'
gem 'pundit',               '~> 2.2.0'
gem 'rack-cors',            '~> 1.1.1'
gem 'rails',                '~> 7.0.2.4'
gem 'rails-i18n',           '~> 7.0.1'
gem 'ransack',              '~> 2.5.0'
gem 'redis',                '~> 4.6.0'
gem 'rqrcode',              '~> 2.0'
gem 'sassc-rails',          '~> 2.1.2'
gem 'sidekiq',              '~> 6.4.2'
gem 'sidekiq-failures',     '~> 1.0.1'
gem 'slim',                 '~> 4.1.0'
gem 'sprockets-rails',      '~> 3.4.2'
gem 'stripe',               '~> 5.53.0'
gem 'turbo-rails',          '~> 1.0.1'

group :development, :test do
  gem 'bullet', '~> 7.0.1'
  gem 'byebug'
  gem 'debug',              '~> 1.4.0',
      platforms: %i[mri mingw x64_mingw]
  gem 'foreman',            '~> 0.87.2'
  gem 'rubocop',            '~> 1.35.0'
  gem 'rubocop-performance', '~> 1.14.3'
  gem 'rubocop-rails',      '~> 2.14.2'
  gem 'rubocop-rspec',      '~> 2.8'
end

group :development do
  gem 'letter_opener',      '~> 1.8.1'
  gem 'spring',             '~> 4.0.0'
  gem 'web-console',        '~> 4.2.0'
end

group :test do
  gem 'ffaker',             '~> 2.20.0'
  gem 'capybara',           '~> 3.36.0'
  gem 'factory_bot_rails'
  gem 'rspec-rails'
  gem 'selenium-webdriver', '~> 4.1.0'
  gem 'webdrivers', '~> 5.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', '~> 2.0.4',
    platforms: %i[mingw mswin x64_mingw jruby]

# Use Kredis to get higher-level data types in Redis [https://github.com/rails/kredis]
# gem "kredis"
