# frozen_string_literal: true

class UserMailer < ApplicationMailer
  layout 'mailer'

  def invite_employee(restaurant_id, email)
    @restaurant = Restaurant.find(restaurant_id)
    @subject = "You have been invited to #{@restaurant.name}"
    @account_link = @restaurant.generate_employee_registration_url(email)

    mail to: email, subject: @subject
  end

  def onboard_new_registration(restaurant_name, registration_url, email)
    @restaurant_name = restaurant_name
    @subject = 'Welcome to Menuizer! 🥳'
    @account_link = registration_url

    mail to: email, subject: @subject
  end
end
