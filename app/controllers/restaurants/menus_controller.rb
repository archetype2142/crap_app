# frozen_string_literal: true

class Restaurants::MenusController < ApplicationController
  layout 'menu_application'
  before_action :set_menu, only: %i[show]

  def show
    @template = @menu.template
    @menu_items = @menu.items
    @categories = @menu.categories
  end

  private

  def set_menu
    @menu = set_restaurant
      .menus
      .find_by(slug: params[:id])
  end

  def set_restaurant
    @restaurant = Restaurant
      .find_by(slug: params[:restaurant_id])
  end
end
