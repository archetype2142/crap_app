# frozen_string_literal: true

class OrdersController < ApplicationController
  def index; end
  def create; end
  def show; end
end
