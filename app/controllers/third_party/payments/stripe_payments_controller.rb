# frozen_string_literal: true

class ThirdParty::Payments::StripePaymentsController < ::ApplicationController
  before_action :authenticate_user!, only: %i[stripe_refresh stripe_return]

  def stripe_refresh; end

  def stripe_return
    redirect_to admin_settings_path
  end

  def create_connected_account
    restaurant = current_restaurant || current_user.restaurants.first
    if restaurant
      connect_account_id = ::Config.find_by(key: 'third_party.stripe.connect_client_id',
                                            restaurant_id: restaurant.id)&.safe_value
    end

    unless connect_account_id
      connect_account_id = StripeGateway.create_connect_account.id
      ::Config.set_config('third_party.stripe.connect_client_id', connect_account_id, 'string',
                          { restaurant_id: restaurant.id })
    end

    @response = StripeGateway.connected_account_link(connect_account_id)

    respond_to do |format|
      format.js { render json: @response }
    end
  end

  def enable_stripe; end
end
