# frozen_string_literal: true

class NewRegistrationsController < ApplicationController
  def create
    Onboarding::NewRegistrationService.new(params: {
                                             email: params[:email],
                                             restaurant_name: params[:restaurant_name]
                                           }).call

    redirect_to root_path, flash: {
      notice: I18n.t('controllers.new_registrations_controller.create.success')
    }
  end
end
