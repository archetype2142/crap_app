# frozen_string_literal: true

class QrRedirectsController < ApplicationController
  def show
    qr_search = QrUrl.find_by(uuid: params[:id])
    redirect_to qr_search.url, allow_other_host: true if qr_search
  end
end
