# frozen_string_literal: true

class AggregatedMenusController < ApplicationController
  layout 'menu_application'
  before_action :set_restaurant, only: :index

  def index
    redirect_to root_path unless @restaurant
  end

  private

  def set_restaurant
    @restaurant = Restaurant.find_by(slug: params[:id])
  end
end
