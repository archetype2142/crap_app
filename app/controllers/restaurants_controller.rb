# frozen_string_literal: true

class RestaurantsController < ApplicationController
  before_action :set_restaurant, only: %i[show aggregated_menu]
  
  def show
    @menus = restaurant.menus
  end

  def aggregated_menu
    @single_menu_mode = Config.get_config('core.single_menu_mode', restaurant_id: @restaurant.id)
  end

  private

  def set_restaurant
    @restaurant = Restaurant.find_by(slug: params[:id])
  end
end
