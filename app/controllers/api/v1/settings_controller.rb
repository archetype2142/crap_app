# frozen_string_literal: true

class Api::V1::SettingsController < ApplicationController
  def show; end
  def update; end
end
