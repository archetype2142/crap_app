# frozen_string_literal: true

class Api::V1::UberOauthController < ApplicationController
  # before_action :validate_token, only: %i[login]

  def login
    # if login_result.failure?
    #   return head :unauthorized
    # end

    # ::Auth::TokenResponseRenderer.
    #   new(user: login_result.data, controller: self).call
  end

  private

  def login_result
    @login_result ||= ::Auth::LoginWithSso
                      .new(
                        token_decoder,
                        login_validator_class: ::Uber::LoginValidator,
                        restaurant_finder_class: ::Uber::CompanyFinder,
                        provider: 'google'
                      ).call
  end

  def token_decoder
    @token_decoder ||= ::Uber::IdTokenDecoder
                       .new(headers['HTTP_AUTHORIZATION']&.split(' ')&.last)
  end

  def validate_token
    return if ::UberAuth::VerifyIdToken.new(token_decoder).call

    head :unauthorized
  end
end
