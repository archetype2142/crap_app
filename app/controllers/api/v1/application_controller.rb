# frozen_string_literal: true

class Api::V1::ApplicationController < ActionController::API
  include Pundit
  include PunditWrapper

  rescue_from ::Pundit::NotAuthorizedError do |_error|
    render_unauthorized
  end

  def render_expected_error(error, key, status = :unprocessable_entity)
    message = if error.respond_to? :message
                error.message
              else
                error.to_s
              end
    render json: {
      errors: {
        key => [message]
      },
      message: message
    }, status: status
  end

  def render_unauthorized
    render_expected_error('You are not authorized to access this resource',
                          :access, :unauthorized)
  end
end
