# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Locales
  include Pundit::Authorization
  include PunditWrapper
  include Turbo::Redirection
  include RedirectHelper

  helper_method :current_restaurant

  layout 'application'

  def after_sign_in_path_for(resource)
    if resource.is_a? AdminUser
      menu_adm_root_path
    else
      admin_root_path
    end
  end

  def active_admin_access_denied(*_args)
    redirect_to '/menu_adm/logout', flash: { notice: I18n.t('controllers.unauthorized') }
  end

  def current_restaurant
    if params[:restaurant_id]
      found_restaurant = current_user.restaurants.find_by(id: params[:restaurant_id])
      current_user.update!(current_restaurant_id: found_restaurant.id) if found_restaurant
      return found_restaurant if found_restaurant
    end

    unless current_user.current_restaurant_id.nil?
      result = current_user.restaurants.find_by(id: current_user.current_restaurant_id)
    end
    return result if result

    current_user.restaurants.first
  end

  rescue_from ::Pundit::NotAuthorizedError do |_error|
    render_unauthorized(admin_root_path)
  end

  def render_expected_error(page, error, _key, _status = :unprocessable_entity)
    message = if error.respond_to? :message
                error.message
              else
                error.to_s
              end

    redirect_to page, flash: {
      error: message
    }
  end

  def render_unauthorized(page)
    render_expected_error(page, 'You are not authorized to access this resource',
                          :access, :unauthorized)
  end
end
