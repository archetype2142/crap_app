# frozen_string_literal: true

class Admin::SearchesController < ::Admin::ApplicationController
  def index
    @resources = Kaminari.paginate_array(@fetch_items + @fetch_restaurants + @fetch_menus)
                         .page(params[:page])
                         .per(params[:per])
  end

  private

  def fetch_restaurants
    @fetch_restaurants ||= current_user.restaurants
                                       .search(name_cont: params[:q])
                                       .result
  end

  def fetch_menus
    @fetch_menus ||= Menu.all
                         .where(restaurant_id: @fetch_restaurants.pluck(:id))
                         .search(title_cont: params[:q])
                         .result
  end

  def fetch_items
    @fetch_items ||= Item.all
                         .includes(:string_translations)
                         .where(menu_id: @fetch_menus.pluck(:id))
                         .search(title_cont: params[:q])
                         .result
  end
end
