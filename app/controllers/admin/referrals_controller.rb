# frozen_string_literal: true

class Admin::ReferralsController < ::Admin::ApplicationController
  POLICY_CLASS = ReferralPolicy
  before_action :set_referral, only: %i[update resend_referral_email]

  def create
    authorize current_restaurant, :create?

    @referral = ::Referrals::Create.new(params: referral_params).call

    error_or_redirect(
      object: @referral,
      success_path: admin_employees_path(restaurant_id: params[:restaurant_id], q: params[:q]),
      failure_path: admin_employees_path(restaurant_id: params[:restaurant_id], q: params[:q]),
      i18n_scope: 'controllers/admin/referrals_controller'
    )
  end

  def update
    authorize((params[:restaurant_id] || current_restaurant.id), :update?)

    @referral = ::Referrals::Update.new(
      params: referral_params,
      referral: @set_referral
    ).call

    error_or_redirect(
      object: @referral,
      success_path: admin_employees_path(restaurant_id: params[:restaurant_id], q: params[:q]),
      failure_path: admin_employees_path(restaurant_id: params[:restaurant_id], q: params[:q]),
      i18n_scope: 'controllers/admin/referrals_controller'
    )
  end

  def resend_referral_email
    authorize(current_restaurant, :update?)

    @referral = ::Referrals::SendNewInviteEmail.new(
      restaurant_id: @set_referral.relate_resource.id,
      email: @set_referral.email
    ).call

    redirect_to admin_employees_path(
      restaurant_id: params[:restaurant_id],
      q: params[:q]
    ),
                flash: { notice: I18n.t("controllers.admin. \
                                                              referrals_controller. \
                                                              resend_referral_email. \
                                                              notice") }
  end

  private

  def set_referral
    @set_referral ||= ::Referral.find_by(id: params[:id])
  end

  def referral_params
    params.require(:referral).permit(
      :first_name,
      :last_name,
      :email,
      :sender_id,
      :relate_resource_id,
      :relate_resource_type,
      :type
    )
  end
end
