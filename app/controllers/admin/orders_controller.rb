# frozen_string_literal: true

class Admin::OrdersController < ::Admin::ApplicationController
  POLICY_CLASS = OrderPolicy
  before_action :set_order, only: %i[update destroy show]

  def index
    authorize((params[:restaurant_id] || current_restaurant.id), :index?)

    @q = current_restaurant.orders.ransack params[:q]
    @orders = @q.result.page(params[:page]).per(params[:per] || 5)
  end

  def update; end

  def show; end

  def destroy; end
end
