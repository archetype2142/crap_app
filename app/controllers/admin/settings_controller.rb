# frozen_string_literal: true

class Admin::SettingsController < ::Admin::ApplicationController
  def index; end

  def link_stripe_account; end
end
