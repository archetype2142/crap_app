# frozen_string_literal: true

class Admin::ApplicationController < ::ApplicationController
  include Locales
  include Pundit::Authorization
  include PunditWrapper

  before_action :authenticate_user!

  layout 'admin_application'
end
