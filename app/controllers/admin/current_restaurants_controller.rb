# frozen_string_literal: true

class Admin::CurrentRestaurantsController < ::Admin::ApplicationController
  def update
    if current_user.restaurants.find(params[:id])
      current_user.update!(current_restaurant_id: params[:id])
    end

    render json: { success: true }
  end
end
