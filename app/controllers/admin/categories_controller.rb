# frozen_string_literal: true

class Admin::CategoriesController < ::Admin::ApplicationController
  before_action :set_category, only: %i[update destroy]

  POLICY_CLASS = CategoryPolicy

  def index
    @q = current_restaurant.categories.ransack params[:q]
    @q.sorts = 'id desc' if @q.sorts.empty?
    @categories = @q
                  .result
                  .page(params[:page])
                  .per(params[:per])
  end

  def create
    authorize current_restaurant, :create?
    @category = Categories::Create.new(params: category_params).call

    error_or_redirect(
      object: @category,
      success_path: admin_categories_path(q: params[:q]),
      failure_path: admin_categories_path,
      i18n_scope: 'controllers/admin/categories_controller'
    )
  end

  def update
    authorize current_restaurant, :update?
    @category = Categories::Update.new(params: category_params, category: @set_category).call

    error_or_redirect(
      object: @category,
      success_path: admin_categories_path(q: params[:q]),
      failure_path: admin_categories_path,
      i18n_scope: 'controllers/admin/categories_controller'
    )
  end

  def destroy
    authorize current_restaurant, :destroy?
    @category = Categories::Destroy.new(category: @set_category).call

    error_or_redirect(
      object: @category,
      success_path: admin_categories_path(q: params[:q]),
      failure_path: admin_categories_path,
      i18n_scope: 'controllers/admin/categories_controller'
    )
  end

  def reorder_position
    Categories::ReorderPosition.new(params: params).call

    redirect_to admin_categories_path
  end

  private

  def set_category
    @set_category ||= current_user.categories.find(params[:id])
  end

  def category_params
    params.require(:category).permit(
      :restaurant_id,
      :title,
      :placement_number
    )
  end
end
