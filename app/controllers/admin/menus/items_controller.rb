# frozen_string_literal: true

class Admin::Menus::ItemsController < ::Admin::ApplicationController
  before_action :item, only: %i[show update destroy]

  def index
    @q = menu.items.ransack params[:q]
    @q.sorts = 'id desc' if @q.sorts.empty?
    @items = @q.result.page(params[:page]).per(params[:per] || 5)
  end

  def new
    @item = Item.new
  end

  def show; end

  def create
    # authorize current_restaurant, :create?

    item = Items::Create.new(params: item_params).call

    error_or_redirect(
      object: item,
      success_path: admin_menu_path(params[:menu_id], q: params[:q]),
      failure_path: admin_menu_path(params[:menu_id], q: params[:q]),
      i18n_scope: 'controller.admin.menus.items_controller'
    )
  end

  def update
    item = Items::Update.new(params: item_params, item: @item).call

    error_or_redirect(
      object: item,
      success_path: admin_menu_path(params[:menu_id], q: params[:q]),
      failure_path: admin_menu_path(params[:menu_id], q: params[:q]),
      i18n_scope: 'controller.admin.menus.items_controller'
    )
  end

  def destroy
    item = Items::Destroy.new(item: @item).call

    error_or_redirect(
      object: item,
      success_path: admin_menu_path(params[:menu_id], q: params[:q]),
      failure_path: admin_menu_path(params[:menu_id], q: params[:q]),
      i18n_scope: 'controller.admin.menus.items_controller'
    )
  end

  private

  def menu
    @menu ||= Menu.find(params[:menu_id])
  end

  def item
    @item = menu.items.find(params[:id])
  end

  def item_params
    params.require(:item).permit(
      :title,
      :description,
      :price,
      :ingredients,
      :image,
      :menu_id,
      category_ids: [],
      tag_ids: []
    )
  end
end
