# frozen_string_literal: true

class Admin::EmployeesController < ::Admin::ApplicationController
  before_action :set_employee, only: %i[destroy update]
  POLICY_CLASS = EmployeePolicy

  def index
    authorize current_restaurant, :index?

    @q = current_restaurant.employees.ransack params[:q]
    @q.sorts = 'id desc' if @q.sorts.empty?
    @employees = @q.result.page(params[:page]).per(params[:per] || 5)
  end

  def create
    authorize current_restaurant, :create?

    employee = Employees::Create.new(params: employee_params).call

    error_or_redirect(
      object: employee,
      success_path: admin_employees_path(q: params[:q]),
      failure_path: admin_employees_path(q: params[:q]),
      i18n_scope: 'controller.admin.employees_controller'
    )
  end

  def update
    authorize current_restaurant, :create?

    employee = Employees::Update.new(params: employee_params, employee: @set_employee).call

    error_or_redirect(
      object: employee,
      success_path: admin_employees_path(q: params[:q]),
      failure_path: admin_employees_path(q: params[:q]),
      i18n_scope: 'controller.admin.employees_controller'
    )
  end

  def destroy
    authorize({
                current_restaurant: current_restaurant,
                employee_id: @set_employee.id
              }, :update?)

    employee = Employees::Destroy.new(employee: @set_employee).call

    error_or_redirect(
      object: employee,
      success_path: admin_employees_path(q: params[:q]),
      failure_path: admin_employees_path(q: params[:q]),
      i18n_scope: 'controller.admin.employees_controller'
    )
  end

  private

  def set_employee
    @set_employee ||= Employee.find(params[:id])
  end

  def employee_params
    params.require(:employee).permit(
      :first_name,
      :last_name,
      :email,
      :phone,
      :language,
      restaurant_ids: []
    )
  end
end
