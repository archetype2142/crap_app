# frozen_string_literal: true

class Admin::RestaurantsController < ::Admin::ApplicationController
  POLICY_CLASS = RestaurantPolicy
  before_action :restaurant, only: %i[show update destroy]

  def index
    authorize current_restaurant, :index?

    return redirect_to admin_tables_path if current_user.employee?

    @q = current_user.becomes(Manager).restaurants.ransack params[:q]
    @restaurants = @q.result.page(params[:page]).per(params[:per] || 5)
  end

  def show
    authorize restaurant, :index?
  end

  def new
    @restaurant = Restaurant.new
  end

  def create
    @restaurant = ::Restaurants::Create.new(
      params: restaurant_params
    ).call

    error_or_redirect(
      object: @restaurant,
      success_path: admin_restaurants_path(q: params[:q]),
      failure_path: admin_restaurants_path(q: params[:q]),
      i18n_scope: 'controllers/admin/restaurants_controller'
    )
  end

  def update
    @restaurant = ::Restaurants::Update.new(
      params: restaurant_params,
      restaurant: @restaurant
    ).call

    error_or_redirect(
      object: @restaurant,
      success_path: admin_restaurants_path(q: params[:q]),
      failure_path: admin_restaurants_path(q: params[:q]),
      i18n_scope: 'controllers/admin/restaurants_controller'
    )
  end

  def destroy
    @restaurant = @restaurant.discard

    error_or_redirect(
      object: @restaurant,
      success_path: admin_restaurants_path(q: params[:q]),
      failure_path: admin_restaurants_path(q: params[:q]),
      i18n_scope: 'controllers/admin/restaurants_controller'
    )
  end

  private

  def restaurant
    @restaurant = Restaurant.find(params[:id])
  end

  def restaurant_params
    params.require(:restaurant).permit(
      :name,
      :image,
      :city,
      manager_ids: []
    )
  end
end
