# frozen_string_literal: true

class Admin::TablesController < ::Admin::ApplicationController
  before_action :table, only: %i[destroy update]
  POLICY_CLASS = TablePolicy

  def index
    @q = current_restaurant.tables.ransack params[:q]
    @q.sorts = 'id desc' if @q.sorts.empty?
    @tables = @q.result.page(params[:page]).per(params[:per] || 5)
  end

  def create
    authorize current_restaurant.id, :create?

    table = Tables::Create.new(params: tables_params).call

    error_or_redirect(
      object: table,
      success_path: admin_tables_path(q: params[:q]),
      failure_path: admin_tables_path(q: params[:q]),
      i18n_scope: 'controller.admin.tables_controller.update'
    )
  end

  def update
    authorize({
                current_restaurant: current_restaurant,
                table_id: @table.id
              }, :update?)

    table = Tables::Update.new(params: tables_params, table: @table).call

    error_or_redirect(
      object: table,
      success_path: admin_tables_path(q: params[:q]),
      failure_path: admin_tables_path(q: params[:q]),
      i18n_scope: 'controller.admin.tables_controller.update'
    )
  end

  def destroy
    authorize({
                current_restaurant: current_restaurant,
                table_id: @table.id
              }, :destroy?)

    table = Tables::Destroy.new(table: @table).call

    error_or_redirect(
      object: table,
      success_path: admin_tables_path(q: params[:q]),
      failure_path: admin_tables_path(q: params[:q]),
      i18n_scope: 'controller.admin.tables_controller.update'
    )
  end

  private

  def table
    @table ||= Table.find(params[:id])
  end

  def tables_params
    params.require(:table).permit(
      :name,
      :id,
      :restaurant_id,
      :seats_available
    )
  end
end
