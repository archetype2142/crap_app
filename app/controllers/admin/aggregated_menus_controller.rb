# frozen_string_literal: true

class Admin::AggregatedMenusController < ::Admin::ApplicationController
  before_action :set_restaurant

  def index; end

  def update
    restaurant = Restaurants::Update.new(
      params: aggregated_menu_params, 
      restaurant: @restaurant
    ).call

    error_or_redirect(
      object: restaurant,
      success_path: admin_aggregated_menus_path(q: params[:q]),
      failure_path: admin_aggregated_menus_path,
      i18n_scope: 'controllers/admin/admin_aggregated_menus_controller'
    )
  end

  private

  def set_restaurant
    @restaurant ||= current_restaurant
  end

  def aggregated_menu_params
    params
      .require(:restaurant)
      .permit(
        :restaurant_id,
        :aggregated_menu_title,
        :aggregated_menu_background,
        :aggregated_menu_font,
        :aggregated_menu_footer,
        :aggregated_menu_animation,
        :aggregated_menu_text_color,
        :aggregated_menu_card_color
      )
  end
end
