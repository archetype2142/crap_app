# frozen_string_literal: true

class Admin::MenusController < ::Admin::ApplicationController
  POLICY_CLASS = MenuPolicy
  before_action :set_menu, only: %i[show update destroy]

  def index
    authorize((params[:restaurant_id] || current_restaurant.id), :index?)

    @q = current_restaurant.menus.ransack params[:q]
    @menus = @q.result.page(params[:page]).per(params[:per] || 5)
  end

  def show
    authorize({ restaurant: current_restaurant, menu: @set_menu }, :show?)

    @menu = @set_menu
    @q = @menu.items.ransack params[:q]
    @q.sorts = 'id desc' if @q.sorts.empty?
    @items = @q.result.page(params[:page]).per(params[:per] || 5)

    @fonts = Menu::FONTS
    @items = @q.result.page(params[:page]).per(params[:per] || 10)
  end

  def create
    authorize((params[:restaurant_id] || current_restaurant.id), :create?)

    @menu = ::Menus::Create.new(params: menu_params).call

    error_or_redirect(
      object: @menu,
      success_path: admin_menus_path(restaurant_id: params[:menu][:restaurant_id], q: params[:q]),
      failure_path: admin_restaurant_path(id: params[:menu][:restaurant_id], q: params[:q]),
      i18n_scope: 'controllers/admin/menus_controller'
    )
  end

  def update
    authorize((params[:restaurant_id] || current_restaurant.id), :update?)

    menu = ::Menus::Update.new(params: menu_params, menu: @set_menu).call

    error_or_redirect(
      object: menu,
      success_path: admin_menu_path(menu, q: params[:q]),
      failure_path: admin_menu_path(menu, q: params[:q]),
      i18n_scope: 'controllers/admin/menus_controller'
    )
  end

  def destroy
    authorize((params[:restaurant_id] || current_restaurant.id), :destroy?)

    menu = ::Menus::Destroy.new(menu: @set_menu).call

    error_or_redirect(
      object: menu,
      success_path: admin_menus_path(restaurant_id: params[:restaurant_id], q: params[:q]),
      failure_path: admin_menu_path(menu),
      i18n_scope: 'controllers/admin/menus_controller'
    )
  end

  private

  def set_menu
    @set_menu ||= Menu.find(params[:id])
  end

  def menu_params
    params.require(:menu).permit(
      :background_color,
      :image,
      :font,
      :title,
      :footer_text,
      :qr_code,
      :description,
      :restaurant_id,
      :font_url,
      :font_css_rule,
      :font_color,
      :card_color
    )
  end
end
