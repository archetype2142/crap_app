# frozen_string_literal: true

class Admin::Authentication::SessionsController < ::Devise::SessionsController
  layout 'application'
end
