# frozen_string_literal: true

class Admin::ManagersController < ::Admin::ApplicationController
  POLICY_CLASS = ManagerPolicy
  before_action :set_manager, only: %i[update destroy edit]

  def index
    authorize current_restaurant, :index?

    @q = Manager.all
                .joins(:restaurants)
                .where(restaurants: { id: [current_restaurant.id] })
                .ransack(params[:q])
    @q.sorts = 'id desc' if @q.sorts.empty?
    @managers = @q.result.page(params[:page]).per(params[:per] || 5)
  end

  def new
    @manager = current_user.managers.build
  end

  def update
    authorize current_restaurant, :update?

    manager = Managers::Update.new(
      params: manager_params.except(:email),
      manager: @manager
    ).call

    error_or_redirect(
      object: manager,
      failure_path: edit_admin_manager_path(@manager.id, q: params[:q]),
      success_path: admin_managers_path(q: params[:q]),
      i18n_scope: 'controller.admin.managers_controller'
    )
  end

  def edit; end

  def create
    authorize current_restaurant, :create?

    manager = Managers::Create.new(
      params: manager_params
    ).call

    error_or_redirect(
      object: manager,
      failure_path: admin_managers_path(q: params[:q]),
      success_path: edit_admin_manager_path(id: (manager&.id || -1), q: params[:q]),
      i18n_scope: 'controller.admin.managers_controller'
    )
  end

  def destroy; end

  private

  def set_manager
    @manager = ::Manager.find(params[:id])
  end

  def manager_params
    params.require(:manager).permit(
      :first_name,
      :email,
      :last_name,
      :phone,
      :password,
      :password_confirmation,
      restaurant_ids: []
    )
  end
end
