# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
ActiveAdmin.register Config do
  actions :all, except: %i[destroy new]

  permit_params :location_id,
                :value

  index do
    selectable_column
    id_column
    column :key
    column :key_type
    column :value
    column :location_id
    column :needs_auth
    column :internal
    column :example
    column :description

    column :created_at, width: 200 do |config|
      config.created_at.utc.iso8601
    end
    column :updated_at do |config|
      config.updated_at.utc.iso8601
    end

    actions
  end

  form do |f|
    f.semantic_errors(*f.object.errors.keys)

    f.input :key, input_html: { readonly: true, disabled: true }
    f.input :key_type, input_html: { readonly: true, disabled: true }
    f.input :location_id, as: :select, collection: Location.all.collect { |c| [c.name, c.id] }
    f.input :value
    f.text_node "Default value: <em>#{ApplicationConfigValidator.fetch_default(object.key)}</em>"
    f.input :needs_auth, input_html: { readonly: true, disabled: true }
    f.input :internal, input_html: { readonly: true, disabled: true }
    f.text_node "<h2>Examples of values per type</h2><br/>
<ul>
  <li><strong>array</strong>
    <ul>
      <li><em>[\"foo\",\"bar@baz.com\"]</em>
      <li><em>[1,2,3]</em>
      <li><em>[]</em>
    </ul>
  <li><strong>boolean</strong>
    <ul>
      <li><em>false</em>
      <li><em>true</em>
    </ul>
  <li><strong>country_iso</strong>
    <ul>
      <li><em>\"US\"</em>
      <li><em>\"PL\"</em>
      <li><em>\"GB\"</em>
      <li><em>\"SE\"</em>
    </ul>
  <li><strong>decimal</strong>
    <ul>
      <li><em>12.5</em>
      <li><em>-0.18</em>
    </ul>
  <li><strong>email</strong>
    <ul>
      <li><em>\"foo@bar.com\"</em>
    </ul>
  <li><strong>file_upload</strong>
    <ul>
      <li><em>{\"uuid\":\"224ced30-d3eb-40a4-9829-04357186194e\"}</em>
      <li><em>{}</em>
    </ul>
  <li><strong>hash</strong>
    <ul>
      <li><em>{\"key_here\":\"value_here\",\"another\":1}</em>
      <li><em>{}</em>
    </ul>
  <li><strong>color</strong>
    <ul>
      <li><em>\"#FFBBCC\"</em>
      <li><em>\"#fa0\"</em>
    </ul>
  <li><strong>integer</strong>
    <ul>
      <li><em>-50</em>
      <li><em>0</em>
      <li><em>30</em>
    </ul>
  <li><strong>nonnegative_integer</strong>
    <ul>
      <li><em>0</em>
      <li><em>20</em>
    </ul>
  <li><strong>string</strong>
    <ul>
      <li><em>\"any text possible\"</em>
      <li><em>\"including multi line\"</em>
    </ul>
  <li><strong>url</strong>
    <ul>
      <li><em>\"https://www.google.com/\"</em>
      <li><em>\"http://apple.com\"</em>
    </ul>
</ul>
"
    f.input :description, input_html: { readonly: true, disabled: true }
    f.input :example, input_html: { readonly: true, disabled: true }
    f.actions
  end
end
# rubocop:enable Metrics/BlockLength
