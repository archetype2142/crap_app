# frozen_string_literal: true

ActiveAdmin.register Restaurant do
  permit_params :name, :city, :slug, sidebar_menu_items: []

  index do
    selectable_column
    id_column
    column :name
    column :created_at
    column :managers
    column :city
    actions
  end

  filter :name
  filter :created_at

  form do |f|
    f.inputs do
      f.input :name
      f.input :image

      f.input :city
      f.input :sidebar_menu_items
      f.input :slug
      f.input :managers
    end
    f.actions
  end
end
