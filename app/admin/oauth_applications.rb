# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
ActiveAdmin.register OauthApplication do
  permit_params :name, :client_id, :client_secret, :redirect_uri, :title, :description, :scopes,
                :url
  actions :all, except: %i[destroy update edit]

  index do
    id_column
    column :name
    column :client_id
    column :client_secret
    column :redirect_uri
    column :created_at
    column :updated_at
    actions
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :name
      f.input :description
      f.input :scopes

      f.input :client_id
      f.input :client_secret

      f.input :redirect_uri
      f.input :url
    end
    f.actions
  end
end
# rubocop:enable Metrics/BlockLength
