# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
ActiveAdmin.register Manager do
  permit_params :email, :first_name, :last_name, :phone, :language, :restaurants, :password,
                :password_confirmation

  filter :email
  filter :created_at

  index do
    selectable_column
    id_column

    column :first_name
    column :email
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :first_name
      row :last_name
      row :email
      row :phone

      table_for manager.restaurants.order('name ASC') do
        column 'Restaurants' do |restaurant|
          link_to restaurant.name, [:menu_adm, restaurant]
        end
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :first_name
      f.input :last_name
      f.input :phone
      f.input :language, as: :select, collection: %w[pl en]

      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :restaurants
    end
    f.actions
  end
end
# rubocop:enable Metrics/BlockLength
