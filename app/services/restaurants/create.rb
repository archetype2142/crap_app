# frozen_string_literal: true

class Restaurants::Create
  attr_accessor :params

  def initialize(params:)
    @params = params
  end

  def call
    @restaurant = ::Restaurant.new(@params)

    @restaurant.save
    @restaurant
  end
end
