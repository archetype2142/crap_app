# frozen_string_literal: true

class Restaurants::Update
  attr_accessor :params

  def initialize(params:, restaurant:)
    @params = params
    @restaurant = restaurant || nil
  end

  def call
    @restaurant = Restaurant.find_by(id: @params[:id]) if @restaurant.nil?

    @restaurant.update(@params)
    @restaurant.save
    @restaurant
  end
end
