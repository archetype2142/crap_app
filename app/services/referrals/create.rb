# frozen_string_literal: true

class Referrals::Create
  attr_accessor :params

  def initialize(params:)
    @params = params
  end

  def call
    referral = ::Referral.new(params)

    if referral.save
      ::Referrals::SendNewInviteEmail.new(
        restaurant_id: referral.relate_resource.id,
        email: referral.email
      ).call
    end

    referral
  end
end
