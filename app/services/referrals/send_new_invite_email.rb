# frozen_string_literal: true

class Referrals::SendNewInviteEmail
  def initialize(restaurant_id:, email:)
    @restaurant_id = restaurant_id
    @email = email
  end

  def call
    ::UserMailer.invite_employee(
      @restaurant_id,
      @email
    ).deliver_later
  end
end
