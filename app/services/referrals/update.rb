# frozen_string_literal: true

class Referrals::Update
  attr_accessor :params

  def initialize(params:, referral:)
    @params = params
    @referral = referral
  end

  def call
    @referral = ::Referral.find_by(id: @params[:id]) if @referral.nil?

    @referral.update(@params)
    @referral
  end
end
