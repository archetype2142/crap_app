# frozen_string_literal: true

class Referrals::SendNewRegistrationEmail
  def initialize(restaurant_name:, registration_url:, email:)
    @restaurant_name = restaurant_name
    @email = email
    @registration_url = registration_url
  end

  def call
    ::UserMailer.onboard_new_registration(
      @restaurant_name,
      @registration_url,
      @email
    ).deliver_later
  end
end
