# frozen_string_literal: true

class Items::Update
  attr_accessor :params

  def initialize(params:, item:)
    @params = params
    @item = item || nil
  end

  def call
    @item = ::Item.find_by(id: @params[:id]) if @item.nil?

    @item.update(@params)
    @item.save
    @item
  end
end
