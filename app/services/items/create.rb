# frozen_string_literal: true

class Items::Create
  attr_accessor :params

  def initialize(params:)
    @params = params
  end

  def call
    clean_params(params)

    item = Item.new(params)

    item.save
    item
  end

  def clean_params(params)
    params[:category_ids] = params[:category_ids]&.delete_if(&:blank?)
  end
end
