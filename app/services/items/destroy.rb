# frozen_string_literal: true

class Items::Destroy
  attr_accessor :params

  def initialize(item: nil)
    @item = item
  end

  def call
    @item = Item.find_by(id: params[:id]) if @item.nil?

    @item.destroy
    @item
  end
end
