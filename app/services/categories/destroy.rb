# frozen_string_literal: true

class Categories::Destroy
  attr_accessor :params

  def initialize(category: nil)
    @category = category
  end

  def call
    @category = Category.find_by(id: params[:id]) if @category.nil?

    @category.destroy
    @category
  end
end
