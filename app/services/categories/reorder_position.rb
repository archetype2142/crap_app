# frozen_string_literal: true

class Categories::ReorderPosition
  attr_accessor :params

  def initialize(params:)
    @params = params
  end

  def call
    category_positions = params
                          .to_unsafe_h["categories"]
                          .map { |cat| { id: cat["category_id"], placement_number: cat["index"] } }
                          .index_by { |cat| cat[:id] }
    Category.update(category_positions.keys, category_positions.values)
  end
end
