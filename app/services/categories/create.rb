# frozen_string_literal: true

class Categories::Create
  attr_accessor :params

  def initialize(params:)
    @params = params
  end

  def call
    category = Category.new(params)

    category.save
    category
  end
end
