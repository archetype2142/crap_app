# frozen_string_literal: true

class Categories::Update
  attr_accessor :params, :category

  def initialize(params:, category:)
    @params = params
    @category = category || nil
  end

  def call
    @category = Category.find_by(id: @params[:id]) if @category.nil?

    @category.update(@params)

    @category
  end
end
