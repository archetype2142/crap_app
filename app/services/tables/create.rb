# frozen_string_literal: true

class Tables::Create
  attr_accessor :params

  def initialize(params:)
    @params = params
  end

  def call
    table = Table.new(params)

    table.tap do |t|
      t.table_uuid = SecureRandom.uuid
    end

    table.save
    table
  end
end
