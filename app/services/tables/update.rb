# frozen_string_literal: true

class Tables::Update
  attr_accessor :params

  def initialize(params:, table:)
    @params = params
    @table = table || nil
  end

  def call
    @table = Table.find_by(id: @params[:id]) if @table.nil?

    @table.update(@params)
    @table.save
    @table
  end
end
