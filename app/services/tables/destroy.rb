# frozen_string_literal: true

class Tables::Destroy
  attr_accessor :params

  def initialize(table: nil)
    @table = table
  end

  def call
    @table = Table.find_by(id: params[:id]) if @table.nil?

    @table.destroy
    @table
  end
end
