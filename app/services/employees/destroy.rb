# frozen_string_literal: true

class Employees::Destroy
  attr_accessor :params

  def initialize(employee: nil)
    @employee = employee
  end

  def call
    @employee = Employee.find_by(id: params[:id]) if @employee.nil?

    @employee.discard
    @employee
  end
end
