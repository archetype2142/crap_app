# frozen_string_literal: true

class Employees::Create
  attr_accessor :params

  def initialize(params:)
    @params = params
  end

  def call
    employee = Employee.new(params)

    employee.tap do |emp|
      emp.password = SecureRandom.base64(15) unless emp.password
    end

    employee
  end
end
