# frozen_string_literal: true

class Employees::Update
  attr_accessor :params

  def initialize(params:, employee:)
    @params = params
    @employee = employee || nil
  end

  def call
    @employee = Employee.find_by(id: @params[:id]) if @employee.nil?

    @employee.update(@params)
    @employee.save
    @employee
  end
end
