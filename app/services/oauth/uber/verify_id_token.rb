# frozen_string_literal: true

class Oauth::Uber::VerifyIdToken
  DISCOVERY_DOCUMENT_URL = 'https://accounts.google.com/.well-known/' \
                           'openid-configuration'
  GOOGLE_ISSUERS = %w[accounts.google.com https://accounts.google.com].freeze

  attr_reader :token_decoder, :client_ids, :expires_in

  def initialize(
    token_decoder,
    expires_in: 3600,
    client_ids: Config['core.auth.google_sso.client_ids']
  )
    @token_decoder = token_decoder
    @client_ids = client_ids
    @expires_in = expires_in
  end

  def call
    return false unless token_decoder&.token

    # public key is needed to verify the signature
    JWT.decode(
      token_decoder.token, public_key, true,
      {
        algorithm: token_decoder.algorithm,
        aud: aud, iss: GOOGLE_ISSUERS,
        verify_aud: true, verify_iss: true
      }
    )
    true
  rescue JWT::VerificationError, JWT::ExpiredSignature, JWT::InvalidAudError
    false
  end

  private

  def aud
    client_ids.values
  end

  def keys_set
    #  keys in JWK(JSON Web Key) format
    # {
    #  "keys": [
    #   {...,
    #    "kid": "16f8295b2370dd88de4c22aed85cabf52d2757f5",
    #    "n": "tL3TtFEjtVIlFrMsyRncvS6ZLDRr6PejeQv7hx1k-oX059...",
    #    "e": "AQAB"
    #   },
    #   {...,
    #    "kid": "8472c6590b1778fe529c1bd3a8f181cc2af4b200",
    #    "n": "rIVm3h1WGbvKjmvzrpwPFeyAWIeP3W87z-C9k0YarePIF0...",
    #    "e": "AQAB"
    #   }
    #  ]
    # }
    Rails.cache.fetch(DISCOVERY_DOCUMENT_URL, expires_in: expires_in.seconds) do
      meta = HTTParty.get(DISCOVERY_DOCUMENT_URL)
      keys_url = meta['jwks_uri']
      HTTParty.get(keys_url).to_h['keys']
    end
  end

  def public_key
    # finding the key token was signed by kid
    key_info = keys_set
               .detect { |info| info['kid'] == token_decoder.token_kid }

    # creating RSA public key using modulus(n) and an exponent(e)
    decoded_exponent = Base64.urlsafe_decode64(key_info['e'])
    decoded_modulus = Base64.urlsafe_decode64(key_info['n'])
    e = OpenSSL::BN.new(decoded_exponent, 2)
    n = OpenSSL::BN.new(decoded_modulus, 2)

    OpenSSL::PKey::RSA.new.set_key(n, e, nil).public_key
  end
end
