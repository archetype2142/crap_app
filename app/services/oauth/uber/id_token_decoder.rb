# frozen_string_literal: true

class Oauth::Uber::IdTokenDecoder
  attr_accessor :token

  def initialize(token)
    @token = token
  end

  def decoded_token
    @decoded_token ||= JWT.decode(token, nil, false)
  end

  def token_payload
    decoded_token[0]
  end

  def token_header
    decoded_token[1]
  end

  def token_kid
    token_header['kid']
  end

  def algorithm
    token_header['alg']
  end

  def email
    @email ||= token_payload['email']
  end

  def name
    @name ||= token_payload['name']
  end

  def first_name
    @first_name ||= token_payload['given_name']
  end

  def last_name
    @last_name ||= token_payload['family_name']
  end

  def external_id
    @external_id ||= token_payload['sub']
  end
end
