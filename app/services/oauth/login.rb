# frozen_string_literal: true

class Oauth::Login
  attr_reader :token_decoder, :login_validator_class,
              :company_finder_class, :provider

  def initialize(
    token_decoder,
    login_validator_class:,
    company_finder_class:,
    provider:
  )
    @token_decoder = token_decoder
    @login_validator_class = login_validator_class
    @company_finder_class = company_finder_class
    @provider = provider
  end

  def call
    # return login_validator_result if login_validator_result.failure?

    # user = try_find_user

    # return merge_existing_user(user) if joined_not_sso_user?(user)
    # return Result.success(user) if user&.joined_status?
    # return merge_and_join_existing_user(user) if user

    # create_user
  end

  private

  def merge_existing_user(user)
    ::Auth::Sso::MergeExistingUser
      .new(user: user, token_decoder: token_decoder, provider: provider).call
  end

  def merge_and_join_existing_user(user)
    ::Auth::Sso::MergeAndJoinExistingUser
      .new(user: user, user_params: user_params,
           token_decoder: token_decoder, provider: provider).call
  end

  def create_user
    ::Auth::Sso::CreateUser
      .new(company: company, user_params: user_params,
           token_decoder: token_decoder, provider: provider).call
  end

  def company
    @company ||= company_finder_class.new(token_decoder: token_decoder).find
  end

  def try_find_user
    UserIdentifier
      .find_by(provider: provider, external_id: token_decoder.external_id)&.user ||
      User.with_unviewable.not_deleted.find_by(email: token_decoder.email)
  end

  def joined_not_sso_user?(user)
    user&.joined_status? &&
      !user.user_identifiers.find_by(provider: provider)
  end

  def login_validator_result
    @login_validator_result ||= login_validator_class
                                .new(token_decoder: token_decoder, company: company).validate
  end

  def user_params
    member_params = {
      first_name: token_decoder.first_name,
      last_name: token_decoder.last_name,
      email: token_decoder.email,
      company_id: company.id,
      role: 'user',
      visibility: User.get_default_user_visibility(company.id),
      status: 'joined'
    }
    ActionController::Parameters.new({ user: member_params }).permit!
  end
end
