# frozen_string_literal: true

class Orders::Create
  attr_accessor :params

  def initialize(params:)
    @params = params
  end

  def call
    order = ::Order.new(params)

    order.tap do |o|
      o.order_uuid = SecureRandom.uuid
    end
    order.save
    order
  end
end
