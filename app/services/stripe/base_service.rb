# frozen_string_literal: true

require 'net/http'

module Stripe
  class BaseService
    include ActiveModel::Model
    StripeClientError = Class.new(StandardError)
    ClientIdNotFound = Class.new(StandardError)

    attr_accessor(:restaurant)

    def initialize(restaurant: nil)
      @restaurant = restaurant

      set_api_key(restaurant)
    end


    private 

    def set_api_key(restaurant)
      client_id = restaurant.configs.find_by(key: 'third_party.stripe.connect_client_id')
      raise ClientIdNotFound.new unless client_id

      Stripe.api_key = client_id
    end
  end
end
