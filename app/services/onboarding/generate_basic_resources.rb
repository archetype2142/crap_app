# frozen_string_literal: true

class Onboarding::GenerateBasicResources
  attr_accessor :user

  def initialize(user:)
    @user = user
  end

  def call
    managerize_user @user

    @restaurant = create_restaurant

    Setup::BasicRestaurantAccountSetup.new(
      restaurant: @restaurant
    ).call
  end

  private

  def managerize_user(user)
    user.update(type: 'Manager')
  end

  def create_restaurant
    restaurant = ::Restaurant.new(
      name: 'Placeholder',
      sidebar_menu_items: ['']
    )

    restaurant.save!
    restaurant.managers << @user.becomes(Manager)
    restaurant
  end
end
