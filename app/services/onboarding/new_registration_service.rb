# frozen_string_literal: true

class Onboarding::NewRegistrationService
  attr_accessor :params

  def initialize(params:)
    @params = params
  end

  def call
    email = @params[:email]
    restaurant_name = @params[:restaurant_name]

    token = SecureRandom.urlsafe_base64
    ::UserRegistrationToken.create!(
      email: email,
      validity: 24.hours.from_now,
      token: token,
      new_restaurant: true
    )

    Referrals::SendNewRegistrationEmail.new(
      restaurant_name: restaurant_name,
      registration_url: Rails.application
                          .routes
                          .url_helpers
                          .new_user_registration_url(
                            registration_token: token
                          ),
      email: @params[:email]
    ).call
  end
end
