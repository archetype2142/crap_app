# frozen_string_literal: true

class Managers::Create
  attr_accessor :params

  def initialize(params:)
    @params = params
  end

  def call
    manager = Manager.new(params)

    manager.save
    manager
  end
end
