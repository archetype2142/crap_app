# frozen_string_literal: true

class Managers::Update
  attr_accessor :params

  def initialize(params:, manager:)
    @params = params
    @manager = manager || nil
  end

  def call
    @manager = ::Manager.find_by(id: @params[:id]) if @manager.nil?

    @manager.update(@params)
    @manager.save
    @manager
  end
end
