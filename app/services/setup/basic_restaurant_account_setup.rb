# frozen_string_literal: true

class Setup::BasicRestaurantAccountSetup
  def initialize(restaurant:)
    @restaurant = restaurant
    @manager = restaurant.managers.first
  end

  def call
    raise StandardError, 'Only valid for new restaurant setup' if @restaurant.managers.count > 1

    restaurant.menus << Menus::Create.new(title: 'New menu').call

    @restaurant.menus.build(name: 'new menu').save!
    @restaurant.update!(
      sidebar_menu_items: available_menu_items(@restaurant.id)
    )
  end

  def available_menu_items(restaurant_id)
    qr_code_menu_only = Config.get_config('core.qr_code_menu_only_mode',
                                          restaurant_id: restaurant_id)
    unique_table_mode = Config.get_config('core.unique_table_mode', restaurant_id: restaurant_id)
    multi_restaurant_mode = Config.get_config('core.multi_restaurant_mode',
                                              restaurant_id: restaurant_id)
    dashboard_enabled = Config.get_config('core.dashboard_enabled', restaurant_id: restaurant_id)
    multi_employee_mode = Config.get_config('core.multi_employee_mode',
                                            restaurant_id: restaurant_id)

    allowed_items = []

    allowed_items.push 'menus' if qr_code_menu_only
    allowed_items.push('restaurants') if multi_restaurant_mode
    allowed_items.push 'tables' unless unique_table_mode
    allowed_items.push 'dashboard' if dashboard_enabled
    allowed_items.push('employees', 'managers') if multi_employee_mode

    allowed_items
  end
end
