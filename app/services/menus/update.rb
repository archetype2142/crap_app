# frozen_string_literal: true

class Menus::Update
  attr_accessor :params

  def initialize(params:, menu:)
    @params = params
    @menu = menu || nil
  end

  def call
    @menu = Menu.find_by(id: @params[:id]) if @menu.nil?

    @menu.update(@params)
    @menu.save
    @menu
  end
end
