# frozen_string_literal: true

class Menus::Destroy
  attr_accessor :params

  def initialize(menu: nil)
    @menu = menu
  end

  def call
    @menu = Menu.find_by(id: params[:id]) if @menu.nil?

    @menu.destroy
    @menu
  end
end
