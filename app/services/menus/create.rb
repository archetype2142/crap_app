# frozen_string_literal: true

class Menus::Create
  attr_accessor :params

  def initialize(params:)
    @params = params
  end

  def call
    menu = Menu.new(params)

    ActiveRecord::Base.transaction do
      menu.generate_slug

      uuid, menu_url, short_url = menu.generate_qr_data
      qr_code_svg = menu.generate_svg(menu_url)

      qr_code = menu.build_qr_code(
        uuid: uuid,
        url: menu_url
      )

      qr_code.qr_file.attach(
        io: StringIO.new(qr_code_svg),
        filename: "#{menu.title.parameterize}.svg",
        content_type: 'image/svg'
      )
      menu.save
    end

    menu
  end
end
