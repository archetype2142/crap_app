document.addEventListener('turbo:load', ()=> {
  var form = document.getElementById('stripe_connected_account');

  form.addEventListener('submit', (event) => {
    event.preventDefault();

    fetch(event.target.action, {
      method: 'POST',
      body: new URLSearchParams(new FormData(event.target))
    }).then((resp) => {
      return resp.json();
    }).then((body) => {
      location.assign(body.url);
    }).catch((error) => {
      
    });
  });
});
