import "@hotwired/turbo-rails";
import "./bulma_methods.js";
import "tail.select.js/js/tail.select.js";

document.addEventListener('turbo:load', ()=> {
  var restaurant_dropdown=document.getElementById("current_restaurant");
  if(restaurant_dropdown) {
    restaurant_dropdown.value = restaurant_dropdown.dataset.currentRestaurant;

    restaurant_dropdown.addEventListener('change', (event)=>{
      event.preventDefault();
      restaurant_dropdown.value = event.target.value;
      postData(('/admin/current_restaurants/' + restaurant_dropdown.value ), {})
      .then(data =>  {
        console.log(data)
        const url = new URL(location.href);
        url.searchParams.set('restaurant_id', event.target.value);

        location.assign(url.search);
      });
    });
  }

  tail.select('.js-multiselect', {
    'multiple': true,
    'search': true,
    'locale': "#{I18n.locale}",
    'openAbove': false,
    'searchFocus': true
  });

  if (window.innerWidth < 1024) {
    document.querySelector('.navigation .has-dropdown').addEventListener("click", function(e) {
      this.children(".navbar-dropdown").toggle();
    });
  }
});

async function postData(url = '', data = {}) {
  const response = await fetch(url, {
    method: 'PUT',
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json',
      'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
    },
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
    body: JSON.stringify(data)
  });
  return response.json();
}
