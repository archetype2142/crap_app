document.addEventListener('turbo:load', function () {
  var search_form = document.getElementById("search_form_auto_submit");

  search_form.children[0].addEventListener("keydown", (event) => {
    if (event.isComposing || event.keyCode === 229) { return; }
    if (search_form.children[0] >= 3) { search_form.submit(); }
  });
});
