document.addEventListener('turbo:load', function () {
  var fileInputs = document.querySelectorAll('input[type=file]');

  fileInputs.forEach((fileInput, index) => {
    fileInput.addEventListener('change', e => {
      var image_tag;

      if(!(e.target.closest("div#image-field").children[0].childElementCount >= 1)) {
        image_tag = e.target.closest("div#image-field").children[0].appendChild(document.createElement('img'));
        console.log("THIS HAPPENED")
      } else {
        image_tag = e.target.closest("div#image-field").children[0].children[0];
      }

      var reader = new FileReader();

      reader.onload = e => {
        image_tag.src = e.target.result;
      }

      const f = e.target.files[0];
      reader.readAsDataURL(f);
    })
  })
});
