document.addEventListener('turbo:load', function () {
  let bulk_hide_items_button = document.getElementById('hide_items');
  let bulk_destroy_items_button = document.getElementById('destroy_items');
  let selected_boxes = document.querySelectorAll('input:checked');
  let multi_select_box = document.getElementById('select-all-items');

  let toggle = true;

  console.log(bulk_hide_items_button);

  bulk_hide_items_button.addEventListener('click', (event) => {
    event.preventDefault();
    selected_boxes = getSelectedBoxes();
    if(selected_boxes.length == 0) return;



    console.log(selected_boxes.length);
  });

  bulk_destroy_items_button.addEventListener('click', (event) => {
    event.preventDefault();
    selected_boxes = getSelectedBoxes();
    if(selected_boxes.length == 0) return;



    console.log(selected_boxes);
  });

  multi_select_box.addEventListener('click', (event) => {
    event.preventDefault();

    [].forEach.call(document.querySelectorAll(" input[type='checkbox']"), function(el) { el.checked = toggle; });
    toggle = !toggle;
  });

  function getSelectedBoxes() { return document.querySelectorAll('input:checked'); }

  function makeRequest() {

  }
});
