import Sortable from 'sortablejs';

var el = document.getElementById('sortable-items');
var sortable = new Sortable(el, {
  animation: 150,  // ms, animation speed moving items when sorting, `0` — without animation
  easing: "cubic-bezier(1, 0, 0, 1)",
  onEnd: function (evt) {
    var list = [...evt.to.children].map((el, index) => ({index: index, category_id: el.dataset.categoryid}));
    setPosition(list);
  },
});

function setPosition(params) {
  const csrfToken = document.querySelector("[name='csrf-token']").content;

  fetch(window.location.origin + "/admin/categories/reorder-position", {
    method: 'POST',
    headers: {
      'X-CSRF-Token': csrfToken,
      'Content-Type':'application/json' 
    },
    body: JSON.stringify({categories: params})
  }).then(response=>{
    return response.json()
  }).then(data=> 
    // this is the data we get after putting our data,
    console.log(data)
  );
}
