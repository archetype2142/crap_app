# frozen_string_literal: true

class OrderPolicy < ApplicationPolicy
  def index?
    true
    # if @current_user.manager?
    #   @current_user.restaurant_ids.include?(record.to_i)
    # elsif @current_user.employee?
    #   @current_user.restaurant_id == record.to_i
    # end
  end

  def create?
    true
    # @current_user.manager?
  end

  alias show? index?
  alias edit? create?
  alias destroy? create?
  alias update? create?
end
