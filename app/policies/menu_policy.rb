# frozen_string_literal: true

class MenuPolicy < ApplicationPolicy
  def index?
    return false if Config.get_config('core.qr_code_menu_only_mode', restaurant_id: record.to_i)

    if @current_user.manager?
      @current_user.restaurant_ids.include?(record.to_i)
    elsif @current_user.employee?
      @current_user.restaurant_id == record.to_i
    end
  end

  def create?
    @current_user.manager?
  end

  def show?
    return false unless record[:restaurant].menu_ids.include?(record[:menu].id)

    if @current_user.manager?
      @current_user.restaurant_ids.include?(record[:restaurant].id.to_i)
    elsif @current_user.employee?
      @current_user.restaurant_id == record[:restaurant].id.to_i
    end
  end

  alias edit? create?
  alias destroy? create?
  alias update? create?

  class Scope < BaseScope
    def resolve; end
  end
end
