# frozen_string_literal: true

class RestaurantPolicy < ApplicationPolicy
  def index?
    @current_user.restaurant_ids.include?(record.id)
  end

  def create?
    @current_user.restaurant_ids.include?(record.id) if @current_user.manager?
  end

  alias destroy? index?
  alias show? index?
end
