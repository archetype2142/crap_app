# frozen_string_literal: true

class ActiveAdminPunditPolicy < ApplicationPolicy
  def create?
    true
  end
end
