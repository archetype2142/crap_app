# frozen_string_literal: true

class CategoryPolicy < ApplicationPolicy
  def index?
    true
  end

  def create?
    return false unless @current_user.manager?
    return false unless @current_user.restaurant_ids.include?(record.id)

    true
  end

  alias destroy? create?
  alias update? create?
end
