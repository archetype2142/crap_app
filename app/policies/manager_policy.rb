# frozen_string_literal: true

class ManagerPolicy < ApplicationPolicy
  def manage?
    return unless @current_user.manager?

    record.sidebar_menu_items&.include?('managers')
  end

  alias index? manage?
  alias create? manage?
  alias update? manage?
  alias destroy? manage?
end
