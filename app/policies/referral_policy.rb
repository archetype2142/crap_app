# frozen_string_literal: true

class ReferralPolicy < ApplicationPolicy
  def create?
    return false unless @current_user.manager?

    (record.sidebar_menu_items & %w[employees managers]).any?
  end

  alias update? create?
end
