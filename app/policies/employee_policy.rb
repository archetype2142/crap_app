# frozen_string_literal: true

class EmployeePolicy < ApplicationPolicy
  def index?
    record.sidebar_menu_items&.include?('employees')
  end

  def create?
    return false unless @current_user.manager?

    record.sidebar_menu_items&.include? 'employees'
  end

  def update?
    return false unless @current_user.manager?
    return false unless record[:current_restaurant].sidebar_menu_items&.include? 'employees'

    @current_user.restaurant_ids.include?(record[:current_restaurant].id) &&
      record[:current_restaurant].employee_ids.include?(record[:employee_id])
  end

  alias destroy? update?
end
