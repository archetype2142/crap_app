# frozen_string_literal: true

class TablePolicy < ApplicationPolicy
  def index?
    return @current_user.restaurant_ids.include?(record.to_i) if @current_user.manager?
    return (@current_user.restaurant_id == record.to_i) if @current_user.employee?
  end

  def create?
    @current_user.restaurant_ids.include?(record.to_i) if @current_user.manager?
  end

  def update?
    return false unless @current_user.manager?

    @current_user.restaurant_ids.include?(record[:current_restaurant].id.to_i) &&
      record[:current_restaurant].table_ids.include?(record[:table_id])
  end

  alias destroy? update?

  class Scope < BaseScope
    def resolve; end
  end
end
