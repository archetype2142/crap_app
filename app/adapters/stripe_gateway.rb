# frozen_string_literal: true

module StripeGateway
  include StripeGateway::StripeAdapterCore
  include StripeGateway::StripeAdapterLogs
  # include StripeGateway::StripeAdapterErrors
  # include StripeGateway::StripeAdapterHelpers
end
