# frozen_string_literal: true

require 'stripe'

module StripeGateway::StripeAdapterCore
  extend ActiveSupport::Concern
  include ::StripeGateway::StripeAdapterLogs
  # include StripeAdapterErrors

  module ClassMethods
    # rubocop:disable Metrics/ParameterLists
    def create_customer(
      stripe_account_id,
      email: nil,
      name: nil,
      description: nil,
      source: nil,
      on_behalf_of: nil,
      transfer_account_id: nil,
      metadata: {}
    )
      log_info "create_customer: \
          #{email} #{name} #{description} \
          #{source} #{metadata}, account: \
          #{stripe_account_id}/#{on_behalf_of}\
          /#{transfer_account_id}"

      instance_id = ::Config['backend_url'].to_s.strip
                                           .gsub('https://', '')
                                           .gsub('http://', '')
                                           .delete('/')
      instance_id ||= 'Menuizer'

      description = if description.present?
                      "#{instance_id}: #{description}"
                    else
                      "#{instance_id}: Customer: #{email}"
                    end

      stripe_params = {
        email: email.to_s.strip,
        description: description
      }
      extra_stripe_params = {}
      extra_stripe_params[:stripe_account] = stripe_account_id if stripe_account_id.present?

      stripe_params[:name] = name if name.present?
      stripe_params[:metadata] = metadata if metadata.present?
      stripe_params[:source] = source if source.present?
      Stripe::Customer.create(stripe_params, extra_stripe_params)
    end
    # rubocop:enable Metrics/ParameterLists

    ### Connected account methods ###
    ### Ref: https://stripe.com/docs/api/accounts
    def create_connect_account
      Stripe::Account.create(
        type: 'standard'
      )
    end

    def get_account(account_id)
      Stripe::Account.retrieve(account_id)
    end

    def list_accounts
      log_info 'accounts_list'
      all_stripe_accounts = []
      has_more = true
      previous_last = nil

      while has_more
        result = Stripe::Account.list(
          {
            limit: 100,
            starting_after: previous_last
          }
        )
        has_more = result.has_more
        previous_last = result.data.last

        all_stripe_accounts.concat result.data
      end

      all_stripe_accounts
    end

    def connected_account_link(account_id)
      Stripe::AccountLink.create(
        account: account_id,
        refresh_url: ::Config.get_config('third_party.stripe.refresh_url'),
        return_url: ::Config.get_config('third_party.stripe.return_url'),
        type: 'account_onboarding'
      )
    end
  end
end
