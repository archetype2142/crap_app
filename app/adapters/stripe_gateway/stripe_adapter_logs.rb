# frozen_string_literal: true

module StripeGateway::StripeAdapterLogs
  extend ActiveSupport::Concern

  module ClassMethods
    def log_error(msg)
      Rails.logger.error "Error: Stripe: #{msg}".in_red
    end

    def log_info(msg)
      Rails.logger.info "Info: Stripe: #{msg}".in_yellow
    end

    def log(before, after)
      log_info before
      result = yield
      log_info after
      result
    rescue StandardError => e
      log_error(e)
      raise e
    end
  end
end
