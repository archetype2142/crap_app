# frozen_string_literal: true

module Restaurants::Qrcode
  extend ActiveSupport::Concern

  # rubocop:disable Metrics/BlockLength
  included do
    def generate_qr_data
      errors.add(:base, 'Restaurant must be attached') unless restaurant
      errors.add(:base, 'Menu must have a title') unless title

      raise ActiveRecord::RecordNotSaved, errors.full_messages if errors.any?

      uuid = SecureRandom.uuid.to_s
      [
        uuid,
        (ENV.fetch('BASE_URL', nil) + "restaurants/#{restaurant.slug}/menus/" + slug),
        "#{ENV.fetch('BASE_URL', nil)}redirect/#{uuid}"
      ]
    end

    def attach_file(qr_code, title, qr_svg)
      qr_code.qr_file.attach(
        io: StringIO.new(qr_svg),
        filename: "#{title.parameterize}.svg",
        content_type: 'image/svg'
      )
    end

    def generate_svg(url)
      RQRCode::QRCode.new(url).as_svg(
        color: '000',
        shape_rendering: 'crispEdges',
        module_size: 11,
        standalone: true,
        use_path: true,
        viewbox: true
      )
    end
  end
  # rubocop:enable Metrics/BlockLength
end
