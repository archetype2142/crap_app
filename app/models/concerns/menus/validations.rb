# frozen_string_literal: true

module Menus::Validations
  extend ActiveSupport::Concern

  included do
    validates :title, presence: true
    validates :title, uniqueness: { scope: :restaurant_id }
    validate :single_menu_mode, on: :create
  end

  def single_menu_mode
    if Config.get_config('core.qr_code_menu_only_mode',
                         restaurant_id: restaurant_id) && (restaurant.menus.count >= 1)
      errors.add(:base,
                 'single menu mode allows only one menu to be added')
    end
  end
end
