# frozen_string_literal: true

module Menus::Slug
  extend ActiveSupport::Concern

  included do
    def generate_slug
      current_slug = title.parameterize
      current_slug += "-#{SecureRandom.base36}" if restaurant.menus.pluck(:slug).include?(current_slug)

      update_attribute(:slug, current_slug)
      qr_code&.update_attribute(
        :url,
        (ENV.fetch('BASE_URL', nil) + "restaurants/#{restaurant.slug}/menus/" + current_slug)
      )
    end
  end
end
