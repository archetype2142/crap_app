# frozen_string_literal: true

module User::Referrals
  extend ActiveSupport::Concern

  included do
    belongs_to :referral,                 foreign_key: :receiver_id,
                                          inverse_of: :user,
                                          optional: true

    has_many :employee_referrals,         foreign_key: :sender_id,
                                          class_name: 'Referral::EmployeeReferral',
                                          inverse_of: :user,
                                          dependent: :destroy

    has_many :manager_referrals,          foreign_key: :sender_id,
                                          class_name: 'Referral::ManagerReferral',
                                          inverse_of: :user,
                                          dependent: :destroy

    after_create :relate
  end

  def relate(referral_email = email)
    referral = ::Referral.find_by email: referral_email
    referral&.relate self
  end
end
