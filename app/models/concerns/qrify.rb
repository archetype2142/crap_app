# frozen_string_literal: true

module Qrify
  extend ActiveSupport::Concern

  included do
    has_one :qr_code, class_name: '::QrUrl',
                      as: :owner,
                      dependent: :destroy
  end
end
