# frozen_string_literal: true

module Locales
  extend ActiveSupport::Concern

  included do
    before_action :set_locale
  end

  def set_locale
    locale = choose_locale
    locale = :en unless locale&.to_sym&.in?(I18n.available_locales)

    I18n.locale = locale

    cookies[:lang] = locale if params[:locale] && !cookies[:lang]
  end

  # rubocop:disable Metrics/CyclomaticComplexity

  def choose_locale
    i18n_from_param = params[:locale]&.to_sym
    i18n_from_cookie = cookies[:lang]&.to_sym
    i18n_from_account = current_user&.language&.to_sym

    return i18n_from_param if i18n_from_param.in?(I18n.available_locales)
    return i18n_from_cookie if i18n_from_cookie.in?(I18n.available_locales)
    return i18n_from_account if i18n_from_account.in?(I18n.available_locales)
  end

  # rubocop:enable Metrics/CyclomaticComplexity

  def default_url_options
    { locale: I18n.locale }
  end

  def self.default_url_options(options = {})
    options.merge(locale: I18n.locale)
  end
end
