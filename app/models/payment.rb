# frozen_string_literal: true

class Payment < ApplicationRecord
  belongs_to :order

  enum status: { pending: 0, paid: 1, rejected: 2, refunded: 3 }
end
