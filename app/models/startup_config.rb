# frozen_string_literal: true

class StartupConfig
  YML_PATH = "config/startup-#{Rails.env}.yml"

  def self.config
    @config ||= new
  end

  def self.get_config(key, restaurant_id: nil)
    config.get_config(key, restaurant_id: restaurant_id)
  end

  def get_config(key, restaurant_id: nil)
    return Config.get_config(key, restaurant_id: restaurant_id) if ar_connected?

    load_yml
    return yml[key] if yml
  end

  private

  attr_reader :connected, :yml_loaded, :yml

  def ar_connected?
    @ar_connected ||= ActiveRecord::Base.connected?
  end

  def load_yml
    return if yml_loaded

    @yml_loaded = true

    return unless File.exist?(YML_PATH)

    Rails.logger.debug { "Init: Loading config from #{YML_PATH}" }
    @yml = YAML.load_file(YML_PATH)
  end
end
