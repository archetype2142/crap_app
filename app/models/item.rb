# frozen_string_literal: true

class Item < ApplicationRecord
  extend Mobility

  translates  :title,	          type: :string,
                                fallbacks: { pl: :en }
  translates  :description,     type: :text,
                                fallbacks: { pl: :en }
  translates  :ingredients,     type: :text,
                                fallbacks: { pl: :en }
  monetize    :price,           as: :price_cents

  belongs_to 	:menu

  has_many    :item_categories, dependent: :destroy
  has_many    :categories,      class_name: '::Category',
                                through: :item_categories,
                                dependent: :destroy
  has_many    :item_tags,       class_name: '::ItemTag',
                                dependent: :destroy
  has_many    :tags,            class_name: '::Tag',
                                through: :item_tags,
                                dependent: :destroy
  has_many    :line_items,      class_name: '::LineItem',
                                dependent: :destroy
  has_many    :variants,        class_name: '::Variant'

  validates   :price,           numericality: true

  has_one_attached :image do |attachable|
    attachable.variant :thumb, resize_to_limit: [50, 50]
    attachable.variant :menu, resize_to_limit: [75, 75]
  end

  def to_s
    title || nil
  end
end
