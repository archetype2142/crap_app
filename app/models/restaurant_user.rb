# frozen_string_literal: true

class RestaurantUser < ApplicationRecord
  include Discard::Model

  belongs_to :restaurant,  class_name: '::Restaurant'
  belongs_to :user,        polymorphic: true
  validates  :user_id,     uniqueness: { scope: :restaurant_id }

  before_validation :set_type

  def set_type
    self.user_type = user.class.name
  end
end
