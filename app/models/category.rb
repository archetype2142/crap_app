# frozen_string_literal: true

class Category < ApplicationRecord
  extend Mobility

  belongs_to :restaurant, class_name: '::Restaurant'

  has_many :item_categories, dependent: :destroy
  has_many :items, through: :item_categories, dependent: :destroy

  validates :title,	presence: true, uniqueness: { scope: :restaurant_id }

  translates :title, type: :string,
                     fallbacks: { pl: :en }

end
