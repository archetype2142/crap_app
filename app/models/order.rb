# frozen_string_literal: true

class Order < ApplicationRecord
  belongs_to  :restaurant,    class_name: '::Restaurant'
  belongs_to  :table,         class_name: '::Table'

  belongs_to  :employee,      class_name: '::Employee',
                              foreign_key: :user_id,
                              optional: true,
                              inverse_of: :user_id

  has_many    :line_items,    class_name: '::LineItem',
                              dependent: :destroy

  has_many    :payments,      class_name: '::Payment',
                              dependent: :destroy

  enum status: { pending: 0, completed: 1, cancelled: 2, refunded: 3 }

  monetize :tips_total,       as: :tips_total_cents
  monetize :order_total,      as: :order_total_cents
  monetize :discount_amount,  as: :discount_amount_cents

  validates :tips_total,      numericality: true
  validates :order_total,     numericality: true
  validates :discount_amount, numericality: true
end
