# frozen_string_literal: true

require 'uri'
require 'application_config_validator'

class Config < ActiveRecord::Base
  belongs_to :restaurant, optional: true
  validates :key, :key_type, presence: true

  validate :validate_type
  validate :validate_global_key, if: :restaurant_id
  validate :validate_key_collisions, unless: :restaurant_id

  validate :validate_nonnegative_integer, on: %i[create update]
  validate :validate_integer, on: %i[create update]
  validate :validate_color, on: %i[create update]
  validate :validate_file_upload, on: %i[create update]
  validate :validate_url, on: %i[create update]
  validate :validate_boolean, on: %i[create update]
  validate :validate_hash, on: %i[create update]
  validate :validate_array, on: %i[create update]

  after_save :delete_cache

  scope :external, -> { where(internal: false) }
  scope :internal, -> { where(internal: true) }
  scope :no_auth, -> { where(needs_auth: false) }
  scope :needs_auth, -> { where(needs_auth: true) }
  scope :available_in_system_settings, -> { where(in_system_settings: true) }

  # NOTE: we could have it here for safety of development.
  # So that nobody leaks them by accident.
  # default_scope -> { external }

  CACHE_EXPIRATION = 5.minutes
  KEY_TYPES = %w[
    array
    boolean
    country_iso
    decimal
    email
    file_upload
    hash
    color
    integer
    nonnegative_integer
    string
    url
  ].freeze

  def setup_default_attributes(options = {})
    self.key_type = ApplicationConfigValidator.fetch_type(key) if key_type.blank?

    self.internal = if options.key?(:internal)
                      options[:internal]
                    else
                      ApplicationConfigValidator.fetch_internal(key)
                    end

    self.needs_auth = if options.key?(:needs_auth)
                        options[:needs_auth]
                      else
                        ApplicationConfigValidator.fetch_needs_auth(key)
                      end

    self.description = options[:description].presence || ApplicationConfigValidator.fetch_description(key)

    self.example = options[:example].presence || ApplicationConfigValidator.fetch_example(key)
  end

  def self.get_config(key, default_value = nil, restaurant_id: nil)
    # restaurant specific config
    config = get_from_storage(key, restaurant_id: restaurant_id)

    # global config
    config ||= get_from_storage(key, restaurant_id: nil)

    # return value of config from storage
    return config.safe_value unless config.nil?

    # default value given as an argument
    return default_value unless default_value.nil?

    # default config from ApplicationConfigValidator
    app_config = ApplicationConfigValidator::REQUIRED_CONFIG_ENTRIES.find { |c| c[0] == key }
    raise "Invalid config key! #{key}" if app_config.blank?

    app_config.dig(1, :default)
  end

  def self.[](*args)
    get_config(*args)
  end

  def self.get_overwritten_restaurant_ids(key)
    overwritten_restaurant_ids = []
    global_value = get_config(key)
    Restaurant.not_deleted.pluck(:id).each do |restaurant_id|
      if get_config(key, restaurant_id: restaurant_id) != global_value
        overwritten_restaurant_ids << restaurant_id
      end
    end
    overwritten_restaurant_ids
  end

  # Returns nil if the config in the restaurant has no value overwritten.
  # Returns overwritten restaurant specific value if is overwritten.
  def self.get_restaurant_config(key, restaurant_id)
    return nil if restaurant_id.blank?

    global_config = get_config(key)
    restaurant_config = get_config(key, restaurant_id: restaurant_id)
    restaurant_id if global_config != restaurant_config
  end

  def self.set_config(key, value, key_type_override = nil, options = {})
    config = find_or_initialize_by(key: key, restaurant_id: options[:restaurant_id])

    config.key_type    = key_type_override || map_value_type(value)
    config.value       = override_value(config.key_type, value&.to_json)
    config.restaurant_id = options[:restaurant_id].to_i if options[:restaurant_id].present?
    config.setup_default_attributes(options)

    config.save!(validate: options[:validate])
    config
  end

  def self.define!(
    key: nil,
    default: nil,
    key_type: nil,
    internal: false,
    needs_auth: true,
    example: nil,
    description: nil,
    restaurant_id: nil
  )

    existing_config = find_by(key: key, restaurant_id: restaurant_id)
    return existing_config if existing_config.present?

    # create config only if it does not exist
    set_config(
      key,
      default,
      key_type,
      {
        internal: internal,
        needs_auth: needs_auth,
        example: example,
        description: description,
        restaurant_id: restaurant_id,
        validate: false
      }
    )
  end

  def self.find_or_init_in_restaurant(key, restaurant_id)
    config = find_or_initialize_by(key: key, restaurant_id: restaurant_id)
    config.setup_default_attributes

    config
  end

  def self.create_or_update_in_restaurant(key, restaurant_id, value)
    raise "Config restaurant_id empty: #{key}" if restaurant_id.blank?

    global_config = Config.find_by!(key: key, restaurant_id: nil)
    restaurant_config = Config.find_or_init_in_restaurant(global_config.key, restaurant_id)
    restaurant_config.update!(value: value.to_json)
    restaurant_config
  end

  def self.rename_key(old_key, new_key)
    old_c = Config.find_by(key: old_key, restaurant_id: nil)
    new_c = Config.find_by(key: new_key, restaurant_id: nil)
    return unless old_c

    if new_c
      if new_c.value.blank?
        new_c.value = old_c.value
        new_c.key_type = old_c.key_type
      end
      new_c.internal = old_c.internal if new_c.internal.blank?
      new_c.needs_auth = old_c.needs_auth if new_c.needs_auth.blank?
      new_c.description = old_c.description if new_c.description.blank?
      new_c.example = old_c.example if new_c.example.blank?
      new_c.save!
    else
      Config.create(
        key: new_key,
        value: old_c.value,
        internal: old_c.internal,
        needs_auth: old_c.needs_auth,
        key_type: old_c.key_type,
        description: old_c.description,
        example: old_c.example
      )
    end
    old_c.destroy!
  end

  def self.rename_key!(old_key, new_key)
    old_c = Config.find_by(key: old_key, restaurant_id: nil)
    new_c = Config.find_by(key: new_key, restaurant_id: nil)
    return unless old_c

    if new_c
      if old_c.value.present?
        new_c.value = old_c.value
        new_c.key_type = old_c.key_type
      end
      new_c.internal = old_c.internal if old_c.internal.present?
      new_c.needs_auth = old_c.needs_auth if old_c.needs_auth.present?
      new_c.description = old_c.description if old_c.description.present?
      new_c.example = old_c.example if old_c.example.present?
      new_c.save!
    else
      Config.create(
        key: new_key,
        value: old_c.value,
        internal: old_c.internal,
        needs_auth: old_c.needs_auth,
        key_type: old_c.key_type,
        description: old_c.description,
        example: old_c.example
      )
    end
    old_c.destroy!
  end

  # NOTE: DO NOT USE THE METHOD.
  # It destroys physical DB entry and the config
  # would not be able to setup again in SuperAdmin
  #
  # Use .set_config(key, nil) instead
  def self.remove!(key, restaurant_id: nil)
    configs = if restaurant_id.present?
                where(key: key, restaurant_id: restaurant_id)
              else
                where(key: key)
              end

    if configs.present?
      configs.each(&:delete_cache)
      configs.destroy_all
    else
      Rails.cache.delete(key_for_cache(key, restaurant_id))
    end
  end

  def self.override_value(key_type, value)
    return 'false' if key_type == 'boolean' && ['"f"', '"false"'].include?(value)
    return 'true' if key_type == 'boolean' && ['"t"', '"true"'].include?(value)

    value
  end

  def safe_value
    return if value.nil?

    ret = begin
      JSON.parse value
    rescue StandardError
      value
    end
    map_json_element(ret)
  end

  def safe_value_with_type_extras
    sv = safe_value

    # Add file_upload type extra metadata
    if file_upload_type? && sv.is_a?(Hash) && sv['uuid'].present?
      sv[:permalink] = FileUpload.permalink_path(sv['uuid'])
      # NOTE: this would actually make a network request to S3 to prepare the URL
      # So let's cache the response if that's an issue and revalidate on Config.set_config
      sv[:path] = FileUpload.get_path_by_uuid(sv['uuid'])
    end
    sv
  end

  def file_upload_type?
    key_type == 'file_upload'
  end

  def delete_cache
    Rails.cache.delete(self.class.key_for_cache(key, restaurant_id))
  end

  def to_s
    {
      key: key,
      key_type: key_type,
      value: value,
      restaurant_id: restaurant_id,
      internal: internal,
      needs_auth: needs_auth
    }.to_s
  end

  private

  def self.key_for_cache(key, restaurant_id)
    "#{key}:#{restaurant_id.presence || 'global'}"
  end

  def validate_type
    errors.add(:key_type, "Unknown config type: #{key_type}") unless KEY_TYPES.include?(key_type)
  end

  def validate_integer
    return if key_type != 'integer' || value.blank?

    v = JSON.parse(value)

    return if v.is_a?(Integer)

    errors.add(:value,
               "Unknown integer schema: #{value}. Allowed: -infinity..0..infinity natural number")
  rescue JSON::ParserError
    add_json_validation_error(v)
  end

  def validate_nonnegative_integer
    return if key_type != 'nonnegative_integer' || value.blank?

    v = JSON.parse(value)

    return if v.is_a?(Integer) && v.to_i >= 0

    errors.add(:value, "Unknown integer schema: #{value}. Allowed: 0..infinity natural number")
  rescue JSON::ParserError
    add_json_validation_error(v)
  end

  def validate_color
    return if key_type != 'color' || value.blank?

    v = JSON.parse(fixed_color_value(value))

    return if /^#[0-9A-Fa-f]{3}$/.match?(v.to_s)
    return if /^#[0-9A-Fa-f]{6}$/.match?(v.to_s)

    errors.add(:value, "Unknown color schema: #{value}. Allowed: #FFF, #FFAABB")
  rescue JSON::ParserError
    add_json_validation_error(v)
  end

  def validate_file_upload
    return if key_type != 'file_upload' || value.blank?

    v = JSON.parse(value)

    return if v.is_a?(Hash) && v.blank?
    return if v.is_a?(Hash) && v['uuid'].present? && v['uuid'].size == 36

    errors.add(:value, "Unknown file_upload schema: #{value}. Allowed json: {uuid:UUID}")
  rescue JSON::ParserError
    add_json_validation_error(v)
  end

  def validate_url
    return if key_type != 'url' || value.blank?

    v = JSON.parse(value)

    # https://stackoverflow.com/questions/1805761/how-to-check-if-a-url-is-valid
    uri = begin
      URI.parse(v.to_s)
    rescue URI::InvalidURIError
      '-1'
    end

    return if uri.is_a?(URI::HTTP)
    return if uri.is_a?(URI::HTTPS)

    errors.add(:value, "Unknown URL schema: #{value}. Allowed valid URL only")
  rescue JSON::ParserError
    add_json_validation_error(v)
  end

  def validate_boolean
    return if key_type != 'boolean' || value.blank?

    v = JSON.parse(fixed_boolean_value(value))

    return if v.is_a?(TrueClass)
    return if v.is_a?(FalseClass)

    errors.add(:value, "Unknown URL schema: #{value}. Allowed true | false only")
  rescue JSON::ParserError
    add_json_validation_error(v)
  end

  def validate_hash
    return if key_type != 'hash' || value.blank?

    v = JSON.parse(value)

    return if v.is_a?(Hash)

    errors.add(:value, "Unknown hash schema: #{value}. Allowed json: {key: value}")
  rescue JSON::ParserError
    add_json_validation_error(v)
  end

  # TODO: add type of elements? validate them?
  def validate_array
    return if key_type != 'array' || value.blank?

    v = JSON.parse(value)

    return if v.is_a?(Array)

    errors.add(:value, "Unknown hash schema: #{value}. Allowed array: [elem1, elem2...]")
  rescue JSON::ParserError
    add_json_validation_error(v)
  end

  # the idea is to prevent having keys which are prefixes of other keys
  # as it will break the hash creation
  # e.g. when we're trying to save 'core.logic.modules' key
  # it will check whether these do not exist:
  # core.logic.modules.[anything_here]
  # core.logic.modules - plain uniqueness
  # core.logic
  # core
  def validate_key_collisions
    sql_collisions = all_key_prefixes.map do |prefix|
      "key=#{ActiveRecord::Base.connection.quote(prefix)}"
    end
    sql_collisions = sql_collisions << "key LIKE #{ActiveRecord::Base.connection.quote("#{key}.%")}"
    other_configs = Config.where(restaurant_id: restaurant_id).where(sql_collisions.join(' OR '))
    other_configs = other_configs.where.not(id: id) if persisted?
    collision = other_configs.first
    errors.add(:key, "name collision detected: #{collision.key}") if collision
  end

  def validate_global_key
    global_key = Config.find_by(key: key, restaurant_id: nil)

    if global_key.nil?
      errors.add(:key, 'a global key must exist')
    else
      if global_key.internal != internal
        errors.add(:internal,
                   'global key has a different internal setting')
      end
      if global_key.needs_auth != needs_auth
        errors.add(:needs_auth,
                   'global key has a different needs_auth setting')
      end
    end
  end

  def all_key_prefixes
    prefixes = []
    s = key
    until s.blank?
      prefixes << s
      s = s[0, (s.rindex('.') || 0)]
    end
    prefixes
  end

  def map_json_element(ele)
    case ele
    when Hash
      ele.with_indifferent_access
    when Array
      ele.map { |inner_el| map_json_element(inner_el) }
    else
      ele
    end
  end

  # # make it JSON compatible. Useful for our customer support
  # # not needing to provide extra "" around the hex value
  def fixed_color_value(val)
    return val if val.blank?
    return val if val.start_with?('\'') || val.end_with?('\'')
    return val if val.start_with?('"') || val.end_with?('"')

    "\"#{val}\""
  end

  def fixed_boolean_value(val)
    json_parsed = begin
      JSON.parse(val)
    rescue StandardError
      nil
    end
    return 'true' if %w[t true].include?(val) || %w[t true].include?(json_parsed)
    return 'false' if %w[f false].include?(val) || %w[f false].include?(json_parsed)
    return '' if val.nil?

    val
  end

  def add_json_validation_error(val)
    errors.add(:value, "Only JSON value supported: #{val}")
  end

  def self.map_value_type(config_value)
    case config_value.class.name
    when 'TrueClass', 'FalseClass'
      'boolean'
    when 'String'
      config_value.include?('http://') ? 'url' : 'string'
    when *KEY_TYPES
      config_value.class.name.downcase
    else
      'string'
    end
  end

  def self.handle_db_error(err, key, restaurant_id = nil)
    return if $i_am_rake

    Rails.logger.debug { "CONFIG #{key_for_cache(key, restaurant_id)} could not be read: #{err}" }
    raise err
  end

  def self.get_from_storage(key, restaurant_id: nil)
    get_through_redis(key, restaurant_id: restaurant_id)
  rescue StandardError => e
    handle_db_error(e, key, restaurant_id)
  end

  def self.get_from_db(key, restaurant_id: nil)
    find_by(key: key, restaurant_id: restaurant_id)
  end

  def self.get_through_redis(key, restaurant_id: nil)
    Rails.cache.fetch(key_for_cache(key, restaurant_id), expires_in: CACHE_EXPIRATION) do
      get_from_db(key, restaurant_id: restaurant_id)
    end
  rescue StandardError => e
    handle_db_error(e, key, restaurant_id)
  end

  private_class_method :get_through_redis
  private_class_method :get_from_db
  private_class_method :get_from_storage
  private_class_method :handle_db_error
  private_class_method :map_value_type
end
