# frozen_string_literal: true

class Referral < ApplicationRecord
  belongs_to :sender,           class_name: '::User'
  belongs_to :receiver,         class_name: '::User', optional: true
  belongs_to :relate_resource,  polymorphic: true

  validates :email, presence: true,
                    length: {
                      maximum: 60
                    },
                    format: {
                      with: Devise.email_regexp
                    },
                    exclusion: {
                      in: lambda { |object|
                        [object&.sender&.email]
                      }
                    }

  after_commit :send_email, if: :saved_change_to_email?

  scope :related_restaurant_resource, lambda { |restaurant_id, resource_type|
    where(
      'type = ? AND relate_resource_type = ? AND relate_resource_id = ?',
      resource_type,
      'Restaurant',
      restaurant_id
    )
  }

  scope :related_restaurant_resource_pending, lambda { |restaurant_id, resource_type|
    where(
      'type = ? AND relate_resource_type = ? AND relate_resource_id = ? AND receiver_id IS NULL',
      resource_type,
      'Restaurant',
      restaurant_id
    )
  }

  def avatar
    "https://avatars.dicebear.com/v2/initials/#{first_name}-#{last_name}.svg"
  end

  def to_s
    "#{first_name} #{last_name}" || super
  end

  def connected?
    receiver.present?
  end

  def status
    if connected?
      :accepted
    else
      :pending
    end
  end

  def recipient
    User.find_by email: email
  end

  def relate(user = recipient)
    return if user.blank?

    transaction do
      relate_receiver user
      relate_referral user
      yield if block_given?
    end
  end

  def relate_referral(user)
    user.update referral: self
  end

  def relate_receiver(user)
    update receiver: user
  end

  def send_email; end
end
