# frozen_string_literal: true

class StripeMode
  def self.on?(restaurant_id = nil)
    # Support offline development if needed
    return false if ENV['STRIPE_OFF']

    ::StartupConfig.get_config('third_party.stripe.api_key', restaurant_id: restaurant_id).present?
  end

  def self.off?(restaurant_id = nil)
    !on?(restaurant_id)
  end
end
