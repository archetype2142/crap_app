# frozen_string_literal: true

class User < ApplicationRecord
  include ::Discard::Model
  include User::Referrals
  include User::Roles
  ROLES = %w[superadmin admin].freeze

  has_many  :restaurant_users, class_name: '::RestaurantUser',
                               dependent: :destroy

  devise    :database_authenticatable, :registerable,
            :recoverable, :rememberable, :validatable

  enum language: { en: 0, pl: 1 }

  def to_s
    full_name || email.split('@').first || super
  end

  def full_name
    [first_name, last_name].compact.join(' ').presence
  end

  def avatar
    "https://avatars.dicebear.com/v2/initials/#{first_name}-#{last_name}.svg"
  end

  def superadmin?
    manager?
  end

  def manager?
    is_a? Manager
  end

  def employee?
    is_a? Employee
  end

  def user?
    is_a? User
  end
end
