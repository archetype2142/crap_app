# frozen_string_literal: true

class Tag < ApplicationRecord
  has_many :item_tags, dependent: :destroy, class_name: '::ItemTag'
  has_many :items, through: :item_tags, class_name: '::Item'

  has_one_attached :svg
end
