# frozen_string_literal: true

class ItemTag < ApplicationRecord
  belongs_to :tag, class_name: '::Tag'
  belongs_to :item, class_name: '::Item'
end
