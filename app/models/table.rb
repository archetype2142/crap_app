# frozen_string_literal: true

class Table < ApplicationRecord
  belongs_to  :restaurant
  has_many    :orders, class_name: '::Order',
                       dependent: :destroy

  validates :name, length: { maximum: 5, allow_blank: true }

  def to_s
    name || super
  end
end
