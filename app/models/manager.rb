# frozen_string_literal: true

class Manager < User
  has_many  :restaurants,   class_name: '::Restaurant',
                            through: :restaurant_users,
                            dependent: :destroy

  has_many  :categories,    class_name: '::Category',
                            through: :restaurants

  has_many  :menus,         class_name: '::Menu',
                            through: :restaurants

  has_many  :employees,     class_name: '::Employee',
                            through: :restaurants
  validates :restaurant_ids, presence: true
end
