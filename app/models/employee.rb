# frozen_string_literal: true

class Employee < User
  has_many  :orders,        class_name: '::Order',
                            dependent: :destroy
  has_many  :restaurants,   class_name: '::Restaurant',
                            through: :restaurant_users,
                            dependent: :destroy
end
