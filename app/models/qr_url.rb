# frozen_string_literal: true

class QrUrl < ApplicationRecord
  has_one_attached :qr_file
  belongs_to :owner, polymorphic: true, optional: true
end
