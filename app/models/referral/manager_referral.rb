# frozen_string_literal: true

class Referral::ManagerReferral < Referral
  def restaurant
    relate_resource
  end

  def relate(user = recipient)
    super do
      relate_type user
      relate_restaurant user
    end
  end

  def relate_type(user)
    user.update type: 'Manager'
  end

  def relate_restaurant(user)
    user.becomes(Manager).restaurants << restaurant
  end
end
