# frozen_string_literal: true

class Referral::EmployeeReferral < Referral
  def restaurant
    relate_resource
  end

  def relate(user = recipient)
    super do
      relate_type user
      relate_restaurant user
    end
  end

  def relate_type(user)
    user.update type: 'Employee'
  end

  def relate_restaurant(user)
    user.becomes(Employee).restaurants << restaurant
  end
end
