# frozen_string_literal: true

class Menu < ApplicationRecord
  include Qrify
  include Menus::Qrcode
  include Menus::Slug
  include Menus::Validations
  extend Mobility

  translates :description, type: :text
  translates :title, type: :string

  FONTS = ['Nunito', 'Georgia', 'serif', 'Gill Sans', 'sans-serif', 'serif', 'cursive',
           'system-ui'].freeze

  belongs_to :restaurant

  has_many 								:items, dependent: :destroy
  has_many 								:categories,	through: :items

  has_one_attached :image do |attachable|
    attachable.variant :thumb, resize_to_limit: [200, 50]
    attachable.variant :banner, resize_to_limit: [800, 100]
  end

  def to_s
    title.to_s || super
  end
end
