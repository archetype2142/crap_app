class Variant < ApplicationRecord
  extend Mobility

  translates  :title,           type: :string,
                                fallbacks: { pl: :en }
  translates  :description,     type: :text,
                                fallbacks: { pl: :en }
  monetize    :price,           as: :price_cents

  belongs_to  :item,            touch: true, 
                                class_name: '::Item',
                                inverse_of: :variants

  validates   :price,           numericality: true
end
