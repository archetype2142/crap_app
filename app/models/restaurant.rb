# frozen_string_literal: true

class Restaurant < ApplicationRecord
  include Qrify
  include Discard::Model

  has_many    :restaurant_users,    class_name: '::RestaurantUser',
                                    dependent: :destroy

  has_many 	  :managers,            class_name: '::Manager',
                                    through: :restaurant_users,
                                    source: :user,
                                    dependent: :destroy,
                                    source_type: 'Manager'

  has_many    :employees,           class_name: '::Employee',
                                    through: :restaurant_users,
                                    source: :user,
                                    dependent: :destroy,
                                    source_type: 'Employee'

  has_many    :menus,	              class_name: '::Menu',
                                    dependent: :destroy

  has_many    :categories,          class_name: '::Category',
                                    dependent: :destroy

  has_many    :configs,             class_name: '::Config',
                                    dependent: :destroy

  has_many    :tables,              class_name: '::Table',
                                    dependent: :destroy

  has_many    :orders,              class_name: '::Order',
                                    dependent: :destroy

  has_one_attached :image do |attachable|
    attachable.variant :thumb, resize_to_limit: [200, 50]
    attachable.variant :banner, resize_to_limit: [800, 100]
  end

  validates :name, presence: true
  validates :sidebar_menu_items, inclusion: {
    in: %w[ orders tables dashboard
            menus managers employees
            restaurants ]
  }

  after_save :generate_slug

  def to_s
    name.to_s || super
  end

  def generate_employee_registration_url(email)
    token = SecureRandom.urlsafe_base64
    ::UserRegistrationToken.create!(
      email: email,
      validity: 24.hours.from_now,
      token: token
    )

    Rails.application.routes.url_helpers.new_user_registration_url(registration_token: token)
  end

  private

  def generate_slug
    is_unique = Restaurant.where(slug: name.parameterize).count <= 0
    new_slug = if is_unique
                 name.parameterize
               else
                 name.parameterize + SecureRandom.base58
               end

    update_attribute(:slug, new_slug) if saved_change_to_name? && name
  end
end
