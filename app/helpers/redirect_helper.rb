# frozen_string_literal: true

module RedirectHelper
  def error_or_redirect(object:, success_path:, failure_path:, i18n_scope:)
    if object.errors.nil?
      redirect_to success_path, flash: { notice: I18n.t('success', scope: i18n_scope) }
    else
      redirect_to failure_path, flash: { error: object.errors.full_messages }
    end
  end
end
