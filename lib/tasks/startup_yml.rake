# frozen_string_literal: true

namespace :dev do
  desc 'Prepare startup.yml file'
  task prepare_startup_yml: :environment do
    PrepareStartupYml.prepare
  end
end
