# frozen_string_literal: true

require 'application_config_validator'

namespace :dev do
  desc 'Prepare default configs'
  task prepare_configs: :environment do
    ApplicationConfigValidator.define_configs!
  end
end
