# frozen_string_literal: true

require_relative '../app/models/config'

class ApplicationConfigValidator
  REQUIRED_CONFIG_ENTRIES = [
    ['backend_url', { key_type: 'url', default: 'http://localhost:3000', description: 'base url of the application' }],
    ['core.multi_restaurant_mode', { key_type: 'boolean', default: false, description: 'manage multiple restaurants' }],
    ['core.dashboard_enabled', { key_type: 'boolean', default: false, description: 'dashboard' }],
    ['core.multi_employee_mode', { key_type: 'boolean', default: false, description: 'allow user management' }],
    ['core.unique_table_mode', { key_type: 'boolean', default: true, description: 'manage multiple unique tables' }],
    ['core.single_menu_mode', { key_type: 'boolean', default: false, description: 'only one menu that redirect to the rest' }],
    ['core.qr_code_menu_only_mode', { key_type: 'boolean', default: true, description: 'simplified one menu one qr code mode' }],
    ['third_party.stripe.api_key', { key_type: 'string', default: 'sk_test_51KtiQSKPlQ8xvIZQiZh0rOyaUTEsoN2phjPSsemHBx271kCnMqI2421NHOduMeLH7UJCxoba6uBqJUySqUmD6pZ300hHpQAn36', description: 'restaurants stripe api key' }],
    ['third_party.stripe.application_fee_amount', { key_type: 'nonnegative_integer', default: 2, description: 'percentage of amount we charge as application fee per transaction' }],
    ['third_party.stripe.connect_client_id', { key_type: 'string', default: 'abc' }],
    ['third_party.stripe.enabled', { key_type: 'boolean', default: 'false' }],
    ['third_party.stripe.publishable_key', { key_type: 'string', default: 'pk_test_51KtiQSKPlQ8xvIZQWG9o7NCJwRJi1YsTbvuiOov2erI73figoUFqhiRPt3QFHk8nCyB8ROPsKBteRGbyn2ygL4n000aDdlecFh', needs_auth: false }],
    ['third_party.stripe.refresh_url', { key_type: 'url', default: 'http://localhost:3000/third_party/payments/stripe_payments/stripe_refresh', description: 'The URL the user will be redirected to if the account link is expired, has been previously-visited, or is otherwise invalid.' }],
    ['third_party.stripe.return_url', { key_type: 'url', default: 'http://localhost:3000/third_party/payments/stripe_payments/stripe_return', description: 'The URL that the user will be redirected to upon leaving or completing the linked flow.' }],
    ['third_party.uber_eats.client_id', { key_type: 'string', default: '', description: 'Uber client id' }],
    ['third_party.uber_eats.client_secret', { key_type: 'string', default: '', description: 'Uber client secret' }]
  ].freeze

  REQUIRED_CONFIG_ENTRIES_AS_HASH = {}.freeze

  def self.define_configs!
    REQUIRED_CONFIG_ENTRIES.each do |key, options|
      Config.define!(key: key, **options).id
      REQUIRED_CONFIG_ENTRIES_AS_HASH[key] = options
    end

    sync_database_configs_attributes(REQUIRED_CONFIG_ENTRIES)

    REQUIRED_CONFIG_ENTRIES
  end

  def self.fetch_default(key)
    REQUIRED_CONFIG_ENTRIES_AS_HASH[key]&.fetch(:default, nil)
  end

  def self.fetch_type(key)
    REQUIRED_CONFIG_ENTRIES_AS_HASH[key]&.fetch(:type, nil) || 'string'
  end

  def self.fetch_internal(key)
    internal = REQUIRED_CONFIG_ENTRIES_AS_HASH[key]&.fetch(:internal, false)
    internal.nil? ? false : internal
  end

  def self.fetch_needs_auth(key)
    needs_auth = REQUIRED_CONFIG_ENTRIES_AS_HASH[key]&.fetch(:needs_auth, true)
    needs_auth.nil? ? true : needs_auth
  end

  def self.fetch_description(key)
    REQUIRED_CONFIG_ENTRIES_AS_HASH[key]&.fetch(:description, nil)
  end

  def self.fetch_example(key)
    REQUIRED_CONFIG_ENTRIES_AS_HASH[key]&.fetch(:example, nil)
  end

  def self.sync_database_configs_attributes(configs_definitions)
    configs_definitions.each do |key, options|
      sync_database_config(key, options)
    end
  end

  def self.sync_database_config(key, options)
    current_meta = {}
    %i[
      key_type
      internal
      needs_auth
      example
      description
    ].each do |attr|
      current_meta[attr] = options[attr] if options.key?(attr) # even false, '' must be supported
    end

    if options[:default].present?
      # Update value of configs by key, including its global & restaurant overrides
      Config.where('key = ? AND (value IS NULL OR value = ? OR VALUE = ?)', key, '',
                   '""').update_all(value: options[:default])
    end
  end
end
